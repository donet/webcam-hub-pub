<?php defined('SYSPATH') or die('No direct script access.');

return array
(
	// Examples that work
	'example_1' => " this line contains a single quote ' ",
	'example_2' => " this line contains a double quote \" ",
	'example_3' => ' this line contains a single quote " ', 
	'example_4' => 'You have :param1 :param2 to your basket',
	
	// Examples that don't work
	'example_not_working_1' => ' this line contains a single quote \' ', 
	
	'project_title' => 'Wequid',
	'project_copyright' => 'All Rights Reserved. Wequid.',
	'privacy_terms' => 'Privacy and Terms',
);

