<?php defined('SYSPATH') or die('No direct script access.');
 
class Task_CompassCompile extends Minion_Task{

	protected $_options = array(
		
	);
    
	function __construct()
	{
		parent::__construct();
	}

	protected function _execute(array $params)
	{
		$log = Log::instance();
		$orm_obj = ORM::factory(Arr::get($params, 'model', 'Color'));
		$themes_dirname = $orm_obj->getThemeLocation(FALSE);
		$lock_filename = $themes_dirname . 'generate_theme.lock';
		$waiting_time = 5;
		$attempts = 10;
		$attempt = 1;
		$locked = FALSE;
		
		while (!$locked AND $attempt < $attempts) {
			try
			{
				if (file_exists($lock_filename))
				{
					if (posix_getpgid(file_get_contents($lock_filename)) != FALSE)
					{
						exec("ps -ocmd= -p ".posix_getpgid(file_get_contents($lock_file)), $cmd1);
						$old_process_still_running = (preg_match("/^php(.+) --task=CompassCompile/", Arr::get($cmd1, 0, '')) === 1);
					} else
					{
						$old_process_still_running = FALSE;
					}
					if ($old_process_still_running == FALSE)
					{
						// Lock file found, but not used by any process, delete it
						unlink($lock_filename);
					}
				}
				$handle = fopen($lock_filename, "x");
				if ($handle != FALSE)
				{
					$locked = TRUE;
					fwrite($handle, getmypid());
				}
				fclose($handle);
			} catch (Throwable $e)
			{
				$log->add(Log::WARNING, ":error_message", array(':error_message' => $e->getMessage()));
				sleep($waiting_time);
			}
			$attempt++;
		}
		
		if ($locked)
		{
			$exec_out = array();
			exec('compass compile ' . DOCROOT . 'fe' . DIRECTORY_SEPARATOR, $exec_out);
			$log_out = "\n";
			foreach ($exec_out as $exec_out_row)
			{
				$exec_out_row = preg_replace('/\\033\[[0-9]{1,2}m/', '', $exec_out_row);
				$log_out .= "\t".$exec_out_row."\n";
			}
			$log->add(Log::INFO, "Compass compile Result::log_out", array(
				 ':log_out' => $log_out,
			));
		}
		if (file_exists($lock_filename) == TRUE)
		{
			unlink($lock_filename);
		}
	}

}

