<?php defined('SYSPATH') OR die('No direct script access.');

class ORM extends Kohana_ORM {
		/**
	 * Tests if this object has a relationship to a different model,
	 * or an array of different models. When providing far keys, the number
	 * of relations must equal the number of keys.
	 *
	 *
	 *     // Check if $model has the login role
	 *     $model->has('roles', ORM::factory('role', array('name' => 'login')));
	 *     // Check for the login role if you know the roles.id is 5
	 *     $model->has('roles', 5);
	 *     // Check for all of the following roles
	 *     $model->has('roles', array(1, 2, 3, 4));
	 *     // Check if $model has any roles
	 *     $model->has('roles')
	 *
	 * @param  string  $alias    Alias of the has_many "through" relationship
	 * @param  mixed   $far_keys Related model, primary key, or an array of primary keys
	 * @return boolean
	 */
	public function has($alias, $far_keys = NULL)
	{
		$count = $this->count_relations($alias, $far_keys);
		if ($far_keys === NULL)
		{
			return (bool) $count;
		}
		else
		{
			return $count === ((is_array($far_keys) OR $far_keys instanceof Countable) ? count($far_keys) : 1); // This is a fix for php >= 7.2 which needs a countable param for the "count" function
		}

	}
}
