<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Webcam extends ORM {
	
	protected $_table_columns = Array(
		'id' => Array(),
		'name' => Array (),
		'content' => Array(),
		'info_message' => Array(),
		'url' => Array(),
		'company_id' => Array('model' => 'Company'),
		'extra' => Array('list_view'=>FALSE, 'edit_view'=>TRUE),
	);
	
	protected $_serialize_columns = array('extra');
	
	protected $_belongs_to = array(
		'companies' => array(
			'model' => 'Company',
			'foreign_key' => 'company_id',
		),
	);
	
	static $extra_list = array(
		'is_image' => array(
			'type' => 'int',
			'data_type' => 'tinyint',
			'extra' => '',
			'display' => '1',
			'key' => '',
		),
		'image_wheelScroll' => array(
			'type' => 'int',
			'data_type' => 'tinyint',
			'extra' => '',
			'display' => '1',
			'key' => '',
			'default_value' => 'yes',
		),
		'image_wheelScrollValue' => array(
			'type' => 'string',
			'data_type' => 'varchar',
			'extra' => '',
			'key' => '',
			'default_value' => '70',
		),
		'image_animateAutoStart' => array(
			'type' => 'int',
			'data_type' => 'tinyint',
			'extra' => '',
			'display' => '1',
			'key' => '',
			'default_value' => 'yes',
		),
		'image_animateScrollValue' => array(
			'type' => 'string',
			'data_type' => 'varchar',
			'extra' => '',
			'key' => '',
			'default_value' => '1',
			'edit_view' => FALSE,
		),
		'image_animateScrollSpeed' => array(
			'type' => 'string',
			'data_type' => 'varchar',
			'extra' => '',
			'key' => '',
			'default_value' => '5',
			'edit_view' => TRUE,
		),
		'is_out_of_order' => array(
			'type' => 'int',
			'data_type' => 'tinyint',
			'extra' => '',
			'display' => '1',
			'key' => '',
		),
	);
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('extra', $ue);
	}
	
	public function getAjaxData()
	{
		$obj_as_array = $this->as_array();
		
		//if necessary unset
		unset($obj_as_array['extra']);
		
		$list_columns = $this->list_columns();
		$orm_name = str_replace('Model_', '', get_class($this));
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($this->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
			if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'date'))
			{
				if( $obj_as_array[$column_name] != NULL)
				{
					$date = DateTime::createFromFormat('Y-m-d', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $date->format('d-m-Y');
				}
			} else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'time')) {
				if( $obj_as_array[$column_name] != NULL)
				{
					$time = DateTime::createFromFormat('H:i:s', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $time->format('H:i:s');
				}
			} else if ($column_info['type'] == 'string' AND ($column_info['data_type'] == 'datetime' OR $column_info['data_type'] == 'timestamp'))
			{
				if( $obj_as_array[$column_name] != NULL)
				{
					$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $datetime->format('d-m-Y H:i:s');
				}
			} else if ($column_info['type'] == 'string' AND strpos($column_info['data_type'], 'blob') !== FALSE)
			{
				$obj_as_array[$column_name] = Route::url('image', array('field' => 'skirama', 'id' => $this->pk()));
			}
		}
		
		$obj_as_array['is_image'] = ($this->getExtra('is_image') == 'yes');
		if ($obj_as_array['is_image'])
		{
			$obj_as_array['webcamViewer'] = $this->getWebcamViewerSettings();
		}
		$obj_as_array['is_out_of_order'] = ($this->getExtra('is_out_of_order') == 'yes');
		
		
		foreach ($this->has_many() as $column_name=>$column_info) {
			isset($column_info['required']) OR $column_info['required'] = FALSE;
			isset($column_info['edit_view']) OR $column_info['edit_view'] = TRUE;
			$obj_as_array[$column_name] = array();
			foreach ($this->get($column_name)->find_all() as $many_key=>$many_value)
			{
				$obj_as_array[$column_name][] = $many_value->getAjaxData();
			}
		}
		return $obj_as_array;
	}
	
	public function getWebcamViewerSettings()
	{
		$webcamViewer_settings = new StdClass();
		if ($this->getExtra('is_image') == 'yes')
		{
			$webcamViewer_settings->wheelScroll = ($this->getExtra('image_wheelScroll') == 'yes');
			if ($webcamViewer_settings->wheelScroll)
			{
				$webcamViewer_settings->wheelScrollValue = $this->checkIntVal($this->getExtra('image_wheelScrollValue', 70), 70);
			} else
			{
				/*$webcamViewer_settings->wheelZoom = ($this->getExtra('image_wheelZoom') == 'yes');
				if ($webcamViewer_settings->wheelZoom)
				{
					$webcamViewer_settings->wheelZoomMin = $this->getExtra('image_wheelZoomMin');
					$webcamViewer_settings->wheelZoomSpeed = $this->getExtra('image_wheelZoomSpeed');
					$webcamViewer_settings->wheelZoomMax = $this->getExtra('image_wheelZoomMax');
				}*/
			}
			$webcamViewer_settings->animateAutoStart = ($this->getExtra('image_animateAutoStart') == 'yes');
			$webcamViewer_settings->animateScrollValue = $this->checkIntVal($this->getExtra('image_animateScrollValue', 1), 1);
			$animateScrollSpeed = $this->checkIntVal($this->getExtra('image_animateScrollSpeed', 5), 5);
			$webcamViewer_settings->animateScrollSpeed = intval(100/$animateScrollSpeed);
		}
		return $webcamViewer_settings;
	}
	
	public function checkIntVal($value, $default_value)
	{
		try {
			$newValue = intval($value);
			if ($newValue == 0 AND $value !== "0")
			{
				$newValue = $default_value;
			}
		} catch (Exception $e)
		{
			$newValue = $default_value;
		}
		return $newValue;
	}
	
}
