<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Role extends Model_Auth_Role {

	protected $_table_columns = Array(
		'id' => Array(),
		'name' => Array (),
		'description' => Array(),
	);
	
	protected $_has_many = array(
		'users' => array(
			'model' => 'User',
			'through' => 'roles_users',
			'edit_view'=>FALSE
		),
	);

} // End Role Model
