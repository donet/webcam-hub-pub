<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User_Token extends Model_Auth_User_Token {

	protected $_table_columns = Array(
		'id' => Array(),
		'user_id' => Array ('model'=>'User'),
		'user_agent' => Array(),
		'token' => Array(),
		'created' => Array('type'=>'string', 'data_type'=>'timestamp', 'required'=>FALSE),
		'expires' => Array('type'=>'string', 'data_type'=>'timestamp', 'required'=>FALSE),
	);

	protected $_belongs_to = array(
		'user' => array(
			'model' => 'User'
		),
	);

} // End User Token Model
