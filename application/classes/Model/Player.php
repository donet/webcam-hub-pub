<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Player extends ORM {
	
	protected $_table_columns = Array(
		'id' => Array(),
		'skirama' => Array ('required' => FALSE),
		'company_id' => Array('required' => TRUE, 'model' => 'Company'),
		'color_id' => Array('required' => FALSE, 'model' => 'Color'),
		'extra' => Array('list_view'=>FALSE, 'edit_view'=>TRUE),
	);
	
	protected $_serialize_columns = array('extra');
	
	protected $_belongs_to = array(
		'companies' => array(
			'model' => 'Company',
			'foreign_key' => 'company_id',
		),
		'colors' => array(
			'model' => 'Color',
			'foreign_key' => 'color_id',
		),
	);
	
	protected $_has_many = array(
		'webcams' => array(
			'model' => 'Webcam',
			'through' => 'webcams_player',
		),
		'webcams_player' => array(
			'model' => 'WebcamsPlayer',
			'foreign_key' => 'player_id',
			'far_key' => 'player_id',
			'edit_view' => FALSE,
			'list_view' => FALSE,
		),
	);
	
	static $extra_list = array(
		'file_original' => array(
			'type' => 'string',
			'data_type' => 'varchar',
			'extra' => '',
			'key' => '',
		),
		'file_extension' => array(
			'type' => 'string',
			'data_type' => 'varchar',
			'extra' => '',
			'key' => '',
		),
		'slideshowInterval' => array(
			'type' => 'int',
			'data_type' => 'int',
			'extra' => '',
			'key' => '',
		),
		'slideshow_autoStart' => array(
			'type' => 'int',
			'data_type' => 'tinyint',
			'extra' => '',
			'display' => '1',
			'key' => '',
			'default_value' => 'yes',
		),
		'selected_key' => array(
			'type' => 'int',
			'data_type' => 'int',
			'extra' => '',
			'key' => '',
		),
		'icon_size' => array(
			'type' => 'int',
			'data_type' => 'int',
			'extra' => '',
			'key' => '',
		),
		'page_refresh' => array(
			'type' => 'int',
			'data_type' => 'int',
			'extra' => '',
			'key' => '',
		),
	);
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('extra', $ue);
	}
	
	public function getAjaxData()
	{
		$obj_as_array = $this->as_array();
		
		//if necessary unset
		unset($obj_as_array['extra']);
		unset($obj_as_array['color_id']);
		
		$list_columns = $this->list_columns();
		$orm_name = str_replace('Model_', '', get_class($this));
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($this->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
			if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'date'))
			{
				if( $obj_as_array[$column_name] != NULL)
				{
					$date = DateTime::createFromFormat('Y-m-d', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $date->format('d-m-Y');
				}
			} else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'time')) {
				if( $obj_as_array[$column_name] != NULL)
				{
					$time = DateTime::createFromFormat('H:i:s', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $time->format('H:i:s');
				}
			} else if ($column_info['type'] == 'string' AND ($column_info['data_type'] == 'datetime' OR $column_info['data_type'] == 'timestamp'))
			{
				if( $obj_as_array[$column_name] != NULL)
				{
					$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $datetime->format('d-m-Y H:i:s');
				}
			} else if ($column_info['type'] == 'string' AND strpos($column_info['data_type'], 'blob') !== FALSE)
			{
				$obj_as_array[$column_name] = Route::url('image', array('obj' => 'Player', 'field' => 'skirama', 'id' => $this->pk()));
			}
		}
		
		$theme = $this->get('colors')->get('name');
		$obj_as_array['theme'] = ($theme == NULL ? 'default' : $theme);
		
		$obj_as_array['slideshowInterval'] = $this->getExtra('slideshowInterval', 10)*1000;
		$obj_as_array['slideshow_autoStart'] = ($this->getExtra('slideshow_autoStart', Arr::get($this::$extra_list['slideshow_autoStart'], 'default_value')) == 'yes');
		$obj_as_array['selected_key'] = $this->getExtra('selected_key', 0);
		$obj_as_array['icon_size'] = $this->getExtra('icon_size', 130);
		$obj_as_array['page_refresh'] = $this->getExtra('page_refresh', 600);
		
		$obj_as_array['webcams'] = array();
		foreach ($this->get('webcams_player')->order_by('sort', 'ASC')->find_all() as $webcams_player)
		{
			$webcam = new StdClass;
			$webcam = $webcams_player->get('webcam')->getAjaxData();
			$webcam['skirama_position_x'] = $webcams_player->get('position_x');
			$webcam['skirama_position_y'] = $webcams_player->get('position_y');
			if ($webcam['is_out_of_order'] == FALSE)
			{
				$obj_as_array['webcams'][] = $webcam;
			}
		}
		return $obj_as_array;
	}
	
	public static function getResponseHeader($image_type)
	{
		$image_type = 'jpg' AND $image_type = 'jpeg';
		return 'image/'.$image_type;
	}
	
	public function getFileExtension()
	{
		return Arr::get($this->get('extra'), 'file_extension');
	}
	
	public function getImageType()
	{
		switch (strtolower($this->getFileExtension()))
		{
			case 'jpeg': return 'jpg';
			case 'jpg': return 'jpg';
			case 'png': return 'png';
			default: return NULL;
		}
	}
	
	public function modifyImage($params = array())
	{
		$r = new StdClass;
		$r->value = NULL;
		
		$r->image_type = Arr::get($params, 'new_img_type') != NULL ? Arr::get($params, 'new_img_type'): $this->getImageType();
		$width = Arr::get($params, 'width');
		$height = Arr::get($params, 'height');

		if (($width OR $height OR $r->image_type) != NULL)
		{
			$value = $this->get('skirama');
			if ($this->getImageType() == 'jpg' OR $this->getImageType() == 'png' OR $this->getImageType() == 'gif')
			{
				$src_img = imagecreatefromstring($value);
			}
			$src_width = imagesx($src_img);
			$src_height = imagesy($src_img);
			$aspect_ratio = $src_height/$src_width;
			$new_width = $src_width;
			$new_height = $src_height;
			if ($width != NULL)
			{
				$new_width = $width;
				if ($height == NULL) $new_height = abs($new_width * $aspect_ratio);
			}
			if ($height != NULL)
			{
				$new_height = $height;
				if ($width == NULL) $new_width = abs($height / $aspect_ratio);
			}
			$new_img = imagecreatetruecolor($new_width, $new_height);
			if ($r->image_type == 'png' OR $r->image_type == 'gif')
			{
				imagecolortransparent($new_img , imagecolorallocatealpha($new_img , 0, 0, 0, 127));
				imagealphablending($new_img, FALSE);
				imagesavealpha($new_img, TRUE);
			}
			imagecopyresized($new_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $src_width, $src_height);
			ob_start();
			if ($r->image_type == 'jpg')
			{
				imagejpeg($new_img);
			}else if ($r->image_type == 'png')
			{
				imagepng($new_img);
			}
			if ($r->image_type == 'gif')
			{
				imagegif($new_img);
			}
			$r->value = ob_get_contents();
			ob_end_clean();
		}
		return $r;
	}
	
}
