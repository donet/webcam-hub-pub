<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {
	
	protected $_table_columns = Array(
		'id' => Array(),
		'email' => Array (),
		'username' => Array(),
		'password' => Array('list_view'=>FALSE, 'required'=>FALSE),
		'logins' => Array('required'=>FALSE),
		'last_login' => Array('type'=>'string', 'data_type'=>'timestamp', 'required'=>FALSE),
		'extra' => Array('list_view'=>FALSE, 'edit_view'=>FALSE),
	);
	
	protected $_serialize_columns = array('extra');
	
	protected $_has_many = array(
		'user_tokens' => array(
			'model' => 'User_Token'
		),
		'roles' => array(
			'model' => 'Role',
			'through' => 'roles_users',
			'required' => TRUE,
		),
		'companies' => array(
			'model' => 'Company',
			'through' => 'companies_users',
		),
	);
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('extra', $ue);
	}
	
	public function getRoles() {
		$role_list = Array();
		foreach ($this->roles->find_all() as $role) {
			$role_list[] = $role->name;
		}
		return $role_list;
	}
	
	public function getTokens() {
		$token_list = Array();
		foreach ($this->tokens->find_all() as $token) {
			$token_list[] = $token;
		}
		return $role_list;
	}
	
	public function setShowActions()
	{
		$role_list = $this->getRoles();
		if (count(array_intersect(array('superadmin', 'admin', 'editor'), $role_list)) > 0)
		{
			$show_actions = array(
				'add' => TRUE,
				'edit' => TRUE,
				'delete' => TRUE,
			);
		} else
		{
			$show_actions = array(
				'add' => FALSE,
				'edit' => FALSE,
				'delete' => FALSE,
			);
		}
		
		return $show_actions;
	}
	
}
