<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_WebcamsPlayer extends ORM {
	
	protected $_table_name = 'webcams_player';

	protected $_table_columns = Array(
		'id' => Array(),
		'player_id' => Array ('list_view'=>FALSE, 'edit_view'=>FALSE, 'model' => 'Player'),
		'webcam_id' => Array('list_view'=>FALSE, 'edit_view'=>FALSE, 'model' => 'Webcam'),
		'sort' => Array('edit_view'=>FALSE),
		'position_x' => Array(),
		'position_y' => Array(),
		'extra' => Array('list_view'=>FALSE, 'edit_view'=>FALSE),
	);
	
	protected $_serialize_columns = array('extra');
	
	protected $_belongs_to = array(
		'player' => array(
			'model' => 'Player',
			'foreign_key' => 'player_id',
		),
		'webcam' => array(
			'model' => 'Webcam',
			'foreign_key' => 'webcam_id',
		),
	);
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('extra', $ue);
	}
	
	public function getAjaxData()
	{
		$obj_as_array = $this->as_array();
		
		//if necessary unset
		//unset($obj_as_array['deleted']);
		
		$list_columns = $this->list_columns();
		$orm_name = str_replace('Model_', '', get_class($this));
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($this->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
			if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'date'))
			{
				if( $obj_as_array[$column_name] != NULL)
				{
					$date = DateTime::createFromFormat('Y-m-d', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $date->format('d-m-Y');
				}
			} else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'time')) {
				if( $obj_as_array[$column_name] != NULL)
				{
					$time = DateTime::createFromFormat('H:i:s', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $time->format('H:i:s');
				}
			} else if ($column_info['type'] == 'string' AND ($column_info['data_type'] == 'datetime' OR $column_info['data_type'] == 'timestamp'))
			{
				if( $obj_as_array[$column_name] != NULL)
				{
					$datetime = DateTime::createFromFormat('Y-m-d H:i:s', $obj_as_array[$column_name]);
					$obj_as_array[$column_name] = $datetime->format('d-m-Y H:i:s');
				}
			} else if ($column_info['type'] == 'string' AND strpos($column_info['data_type'], 'blob') !== FALSE)
			{
				$obj_as_array[$column_name] = Route::url('image', array('field' => 'skirama', 'id' => $this->pk()));
			}
		}
		foreach ($this->has_many() as $column_name=>$column_info) {
			isset($column_info['required']) OR $column_info['required'] = FALSE;
			isset($column_info['edit_view']) OR $column_info['edit_view'] = TRUE;
			$obj_as_array[$column_name] = array();
			foreach ($this->get($column_name)->find_all() as $many_key=>$many_value)
			{
				$obj_as_array[$column_name][] = $many_value->getAjaxData();
			}
		}
		return $obj_as_array;
	}
	
}
