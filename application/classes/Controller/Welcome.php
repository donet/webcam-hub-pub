<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Auth {

	public function action_index()
	{
		$this->template->title = "Welcome index";
		$this->template->content ='<h1>Welcome '.ucfirst($this->user->get('username')).'</h1>';
	}

} // End Welcome
