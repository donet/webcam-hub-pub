<?php defined('SYSPATH') or die('No direct script access.');

class Controller_WebcamHubManager extends Controller_Auth {

	protected $show_actions;
	protected $dataTable_name;

	public function before() {
		parent::before();
		$user_roles = $this->user->getRoles();
		if (count($user_roles) == 1 AND in_array('login', $user_roles)) {
			$this->redirect(Route::url('default'), NULL, TRUE);
		}
		$this->show_actions = $this->user->setShowActions();
		$this->dataTable_name = Arr::get($_REQUEST, 'dataTable_name', 'table_list');
		if (in_array($this->request->action(), array('save_webcam', 'save_player', 'save_webcamsPlayer', 'save_sort')))
		{
			$cache = Cache::instance();
			$cache->delete_all();
		}
	}
	
	public function action_webcamList() {
		$this->template->title = "Webcam List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'WebcamHubManager';
		$actions = array(
			'save' => 'save_webcam',
			'delete' => 'delete_orm_obj',
		);
		$model = ORM::factory('Webcam');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		$ids = array();
		foreach ($this->user->get('companies')->find_all() as $company)
		{
			$ids[] = $company->pk();
		}
		$model->or_where('company_id', 'IS', NULL)->or_where('company_id', '=', 0);
		if (count($ids) > 0)
		{
			$model->or_where('company_id', 'IN', $ids);
		}
		$orm_objs = $model->find_all();
	}
	
	public function action_playerList() {
		$this->template->title = "Player List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'WebcamHubManager';
		$actions = array(
			'save' => 'save_player',
			'delete' => 'delete_orm_obj',
			'codeGenerator' => 'codeGenerator',
		);
		$model = ORM::factory('Player');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		$ids = array();
		foreach ($this->user->get('companies')->find_all() as $company)
		{
			$ids[] = $company->pk();
		}
		$model->or_where('company_id', 'IS', NULL)->or_where('company_id', '=', 0);
		if (count($ids) > 0)
		{
			$model->or_where('company_id', 'IN', $ids);
		}
		$orm_objs = $model->find_all();
	}
	
	public function action_webcamsPlayerList() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		$this->template->title = "Webcams Player List";
		$list_view = View::factory('user_manager/webcamsPlayerList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$edit_view = View::factory('user_manager/webcamsPlayerEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'WebcamHubManager';
		$actions = array(
			'save' => 'save_webcamsPlayer',
			'delete' => 'delete_orm_obj',
			'sort' => 'save_sort',
		);
		$model = ORM::factory('WebcamsPlayer');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = array(
			'add' => FALSE,
			'edit' => TRUE,
			'delete' => FALSE,
		);
		$dataTable_name = $this->dataTable_name;
		$webcams = Arr::get($_REQUEST, 'webcams');
		if ($webcams != NULL)
		{
			$model->where('webcam_id', 'IN', $webcams);
		}
		$player_id = Arr::get($_REQUEST, 'player_id');
		if ($player_id != NULL)
		{
			$model->where('player_id', '=', $player_id);
		}
		$orm_objs = $model->order_by('sort', 'ASC')->find_all();
		$result->list_view = $list_view->render();
		$result->edit_view = $edit_view->render();
		
		echo json_encode($result);
	}
	
	public function action_colorList() {
		$this->template->title = "Color List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'WebcamHubManager';
		$actions = array(
			'save' => 'save_color',
			'delete' => 'delete_orm_obj',
		);
		$model = ORM::factory('Color');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		/*$ids = array();
		foreach ($this->user->get('companies')->find_all() as $company)
		{
			$ids[] = $company->pk();
		}
		$model->or_where('company_id', 'IS', NULL)->or_where('company_id', '=', 0);
		if (count($ids) > 0)
		{
			$model->or_where('company_id', 'IN', $ids);
		}*/
		$orm_objs = $model->find_all();
	}
	
	public function action_companyList() {
		$this->template->title = "Company List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'WebcamHubManager';
		$actions = array(
			'save' => 'save_company',
			'delete' => 'delete_orm_obj',
		);
		$model = ORM::factory('Company');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		$orm_objs = $model->find_all();
	}
	
	public function action_save_webcam() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('Webcam', $id);
		$name = Arr::get($_REQUEST, 'name');
		$orm_obj->set('name', $name);
		$content = Arr::get($_REQUEST, 'content');
		$orm_obj->set('content', $content);
		$info_message = Arr::get($_REQUEST, 'info_message');
		$orm_obj->set('info_message', $info_message);
		$url = Arr::get($_REQUEST, 'url');
		$orm_obj->set('url', $url);
		$company_id = Arr::get($_REQUEST, 'company_id');
		$orm_obj->set('company_id', $company_id);
		$extra = Arr::get($_REQUEST, 'extra', array());
		$orm_obj->set('extra', array_filter($extra, function($k){return $k != NULL;}));
		
		$orm_obj->save();
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	public function action_save_player() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('Player', $id);
		$company_id = Arr::get($_REQUEST, 'company_id');
		$company_id == '' AND $company_id = NULL;
		$orm_obj->set('company_id', $company_id);
		$color_id = Arr::get($_REQUEST, 'color_id');
		$color_id == '' AND $color_id = NULL;
		$orm_obj->set('color_id', $color_id);
		$extra = Arr::get($_REQUEST, 'extra', array());
		$orm_obj->set('extra', array_filter($extra, function($k){return $k != NULL;}));
		
		if ($_FILES["skirama"]["error"] == UPLOAD_ERR_OK)
		{
			$tmp_name = $_FILES["skirama"]["tmp_name"];
			$orm_obj->set("skirama", file_get_contents($tmp_name));
			$orm_obj->setExtra('file_original', $_FILES["skirama"]["name"]);
			$orm_obj->setExtra('file_extension', pathinfo($_FILES["skirama"]["name"], PATHINFO_EXTENSION));
		} else if (Arr::get($_REQUEST, "skirama".'_canc', 0) == 1){
			$orm_obj->set("skirama", NULL);
		}
		
		$orm_obj->save();
		
		foreach ($orm_obj->has_many() as $key=>$model_value)
		{
			if ($key != 'webcams_player')
			{
				$values = Arr::get($_REQUEST, $key, array());
				if (count($values) > 0) {
					foreach ($values as $value)
					{
						if ($value != '')
						{
							if(!$orm_obj->has($key, $value))
							{
								$orm_obj->add($key, $value);
							}
						}
					}
				}
				$all_many_objs = array();
				foreach (ORM::factory($model_value['model'])->find_all() as $many_obj)
				{
					$all_many_objs[] = $many_obj->pk();
				}
				$many_objs_to_remove = array_diff($all_many_objs, $values);
				if (count($many_objs_to_remove) > 0)
				{
					$orm_obj->remove($key, $many_objs_to_remove);
				}
			}
		}
		
		$sorter = 1;
		foreach ($orm_obj->get('webcams_player')->order_by('sort', 'ASC')->find_all() as $webcams_player)
		{
			$webcams_player->set('sort', $sorter);
			$webcams_player->save();
			$sorter++;
		}
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	public function action_save_webcamsPlayer() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('WebcamsPlayer', $id);
		$position_x = Arr::get($_REQUEST, 'position_x');
		$orm_obj->set('position_x', $position_x);
		$position_y = Arr::get($_REQUEST, 'position_y');
		$orm_obj->set('position_y', $position_y);
		$extra = Arr::get($_REQUEST, 'extra', array());
		$orm_obj->set('extra', array_filter($extra, function($k){return $k != NULL;}));
		
		$orm_obj->save();
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->row_data[] = $orm_obj->get('webcam')->get('name');
		$result->status = 'OK';
		echo json_encode($result);
	}

	public function action_save_color() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('Color', $id);
		$themes_dirname = $orm_obj->getThemeLocation(TRUE);
		//$company_id = Arr::get($_REQUEST, 'company_id');
		//$orm_obj->set('company_id', $company_id);
		$name = Arr::get($_REQUEST, 'name');
		if ($orm_obj->get('name') != '' AND $name != $orm_obj->get('name'))
		{
			$filename = $themes_dirname . $orm_obj->get('name') .  '.scss';
			unlink($filename);
		}
		$orm_obj->set('name', $name);
		$extra = Arr::get($_REQUEST, 'extra', array());
		$orm_obj->set('extra', array_filter($extra, function($k){return $k != NULL;}));
		
		$orm_obj->save();
		
		foreach ($orm_obj->has_many() as $key=>$model_value)
		{
			$values = Arr::get($_REQUEST, $key, array());
			if (count($values) > 0) {
				foreach ($values as $value)
				{
					if ($value != '')
					{
						if(!$orm_obj->has($key, $value))
						{
							$orm_obj->add($key, $value);
						}
					}
				}
			}
			$all_many_objs = array();
			foreach (ORM::factory($model_value['model'])->find_all() as $many_obj)
			{
				$all_many_objs[] = $many_obj->pk();
			}
			$many_objs_to_remove = array_diff($all_many_objs, $values);
			if (count($many_objs_to_remove) > 0)
			{
				$orm_obj->remove($key, $many_objs_to_remove);
			}
		}
		
		$filename = $themes_dirname . $orm_obj->get('name') .  '.scss';
		try
		{
			if (file_exists($filename)) {
				// "The file $filename exists";
				$handle = fopen($filename, "w");
			} else {
				// "The file $filename does not exist";
				$handle = fopen($filename, "x");
			}
		} catch (Throwable $e)
		{
			Kohana_Exception::handler($e);
		}
		$generate_theme = View::factory('frontend/generate_theme')
			->bind('extra_list', $extra_list)
			->bind('extra', $extra);
		$extra_list = isset($orm_obj::$extra_list) ? $orm_obj::$extra_list : array();
		fwrite($handle, $generate_theme->render());
		fclose($handle);

		exec('php ' . MODPATH . 'minion' . DIRECTORY_SEPARATOR .'minion --task=CompassCompile > /dev/null &');
			/*Minion_Task::factory(array(
				'task' => 'your:task',
				'option' => 'value'
			))->execute();*/
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	public function action_save_company() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('Company', $id);
		$name = Arr::get($_REQUEST, 'name');
		$orm_obj->set('name', $name);
		$extra = Arr::get($_REQUEST, 'extra', array());
		$orm_obj->set('extra', array_filter($extra, function($k){return $k != NULL;}));
		
		$orm_obj->save();
		
		if(!$this->user->has('companies', $orm_obj))
		{
			$this->user->add('companies', $orm_obj);
		}
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	public function action_save_sort() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$orm_name = Arr::get($_REQUEST, 'orm_name');
		$ids = Arr::get($_REQUEST, 'ids');
		$positions = Arr::get($_REQUEST, 'positions');
		if ($ids != NULL AND $positions != NULL)
		{
			$list_elem = array_combine ($ids , $positions);
			$result->ids = array();
			foreach ($list_elem as $id => $sort)
			{
				$orm_obj = ORM::factory($orm_name, $id);
				$orm_obj->set('sort', $sort);
				
				$orm_obj->save();
				
				$result->ids[] = $orm_obj->pk();
			}
			
			$result->status = 'OK';
		} else
		{
			$result->message = 'Params are NULL.';
			$result->status = 'KO';
		}
		echo json_encode($result);
	}
	
	protected function get_orm_obj_dt_row($orm_obj) {
		$row_data = array();
		$current_cell = 0;
		$list_columns = $orm_obj->list_columns();
		$table_columns = $orm_obj->table_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($table_columns, $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$list_columns[$column_name] = $column_info;
		}
		foreach ($orm_obj->as_array() as $key=>$value) {
			$column_info = Arr::get($list_columns, $key);
			if ($column_info['list_view'])
			{
				if ($column_info['data_type'] == 'timestamp' AND $value != NULL) {
					$row_data[$current_cell++] = DateTime::createFromFormat('U',$value)->format('Y-m-d H:i:s');
				} else if (strpos($column_info['data_type'], 'blob') !== FALSE) {
					if (isset($value) AND $value != NULL) {
						$row_data[$current_cell++] = "<img src=\"data:image/gif;base64," . base64_encode($value) . "\" style=\"max-width:100%;\" />";
					} else {
						$row_data[$current_cell++] = "no file";
					}
				} else if (is_array($value)) {
					$row_data[$current_cell] = '';
					foreach ($value as $extra_key=>$extra_value)
					{
						$row_data[$current_cell] .= $extra_key.':'.$extra_value.'<hr>';
					}
					$current_cell++;
				} else {
					$row_data[$current_cell++] = $value;
				}
			}
		}
		foreach ($orm_obj->has_many() as $key=>$value) {
			$first = TRUE;
			$row_data[$current_cell] = "";
			foreach ($orm_obj->get($key)->find_all() as $many_key=>$many_value)
			{
				if (!$first)
				{
					$row_data[$current_cell] .= "<br>";
				}
				$row_data[$current_cell] .=  Form::hidden($key.'[]', $many_value->pk());
				$row_data[$current_cell] .=  (array_key_exists('name',$many_value->as_array()) ? $many_value->get('name') : $many_value->pk());
				$first = FALSE;
			}
			$current_cell++;
		}
		return $row_data;
	}
	
	public function action_delete_orm_obj() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		$id = Arr::get($_REQUEST, 'id');
		$orm_name = Arr::get($_REQUEST, 'orm_name');
		if($orm_name != NULL)
		{
			$orm_obj = ORM::factory($orm_name, $id);
			if ($orm_obj->loaded())
			{
				$orm_obj->delete();
				$result->status = 'OK';
			}else
			{
				$result->status = 'KO';
				$result->message = $orm_name.' not found.';
			}
		} else
		{
			$result->status = 'KO';
			$result->message = 'Class not found.';
		}
		echo json_encode($result);
	}
	
	
	public function action_codeGenerator() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		$this->template->title = "Player Code";
		$edit_view = View::factory('frontend/generate_host')
			->bind('title', $this->template->title)
			->bind('player_id', $player_id)
			->bind('base_url', $base_url);
		$player_id = Arr::get($_REQUEST, 'player_id', '');
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$base_url = $protocol.$_SERVER['HTTP_HOST'].URL::base();
		$result->edit_view = $edit_view->render();
		
		echo json_encode($result);
	}
}
?>
