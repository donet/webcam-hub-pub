<?php defined('SYSPATH') or die('No direct script access.');

class Controller_UserManager extends Controller_Auth {

	public function before() {
		parent::before();
		if (!Auth::instance()->logged_in('admin') AND !Auth::instance()->logged_in('superadmin')) {
			$this->redirect(Route::url('default'), NULL, TRUE);
		}
		$this->show_actions = array(
			'add' => TRUE,
			'edit' => TRUE,
			'delete' => TRUE,
		);
		$this->dataTable_name = Arr::get($_REQUEST, 'dataTable_name', 'table_list');
	}
	
	public function action_userList() {
		$this->template->title = "Users List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'UserManager';
		$actions = array(
			'save' => 'save_user',
			'delete' => 'delete_orm_obj',
		);
		$model = ORM::factory('User');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		if (!Auth::instance()->logged_in('superadmin')) {
			$ids = array();
			foreach ($this->user->get('companies')->find_all() as $company)
			{
				$ids[] = $company->pk();
			}
			$roles_user_superadmin = DB::select(array('roles_users.user_id','user_id'))
				->from(array('roles_users', 'roles_users'))
				->join(array('roles', 'roles'))
				->on('roles_users.role_id', '=', 'roles.id')
				->on('roles.name', '=', DB::expr("'superadmin'"));
			$model->join(array($roles_user_superadmin, 'roles_user_superadmin'), 'LEFT')->on('user.id', '=', 'roles_user_superadmin.user_id')
				->join(array('companies_users', 'companies_users'))->on('companies_users.user_id', '=', 'user.id')->distinct(true)
				->where('roles_user_superadmin.user_id', 'IS', NULL);
				if (count($ids) > 0)
				{
					$model->and_where('company_id', 'IN', $ids);
				}
		}
		$orm_objs = $model->find_all();
	}
	
	public function action_roleList() {
		if (!Auth::instance()->logged_in('superadmin')) {
			$this->redirect(Route::url('default'), NULL, TRUE);
		}
		$this->template->title = "Roles List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'UserManager';
		$actions = array(
			'save' => 'save_role',
			'delete' => 'delete_orm_obj',
		);
		$model = ORM::factory('Role');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		$orm_objs = $model->find_all();
	}
	
	public function action_tokenList() {
		if (!Auth::instance()->logged_in('superadmin')) {
			$this->redirect(Route::url('default'), NULL, TRUE);
		}
		$this->template->title = "User Token List";
		$this->template->content = View::factory('user_manager/defaultList')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$this->template->modals = View::factory('user_manager/defaultEdit')
			->bind('title', $this->template->title)
			->bind('show_actions', $show_actions)
			->bind('list_columns', $list_columns)
			->bind('has_many', $has_many)
			->bind('controller', $controller)
			->bind('dataTable_name', $dataTable_name)
			->bind('actions', $actions)
			->bind('orm_name', $orm_name)
			->bind('extra_list', $extra_list)
			->bind('orm_objs', $orm_objs);
		$controller = 'UserManager';
		$actions = array(
			'save' => 'save_token',
			'delete' => 'delete_orm_obj',
		);
		$model = ORM::factory('User_Token');
		$orm_name = str_replace('Model_', '', get_class($model));
		$extra_list = isset($model::$extra_list) ? $model::$extra_list : array();
		$list_columns = $model->list_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($model->table_columns(), $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['edit_view'] = Arr::get($extra_column_info, 'edit_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$column_info['model'] = Arr::get($extra_column_info, 'model', $orm_name);
			$list_columns[$column_name] = $column_info;
		}
		$has_many = $model->has_many();
		$show_actions = $this->show_actions;
		$dataTable_name = $this->dataTable_name;
		$orm_objs = $model->find_all();
	}
	
	public function action_save_user() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('User', $id);
		$username = Arr::get($_REQUEST, 'username');
		$orm_obj->set('username', $username);
		$email = Arr::get($_REQUEST, 'email');
		$orm_obj->set('email', $email);
		$logins = Arr::get($_REQUEST, 'logins', 0);
		$logins == '' AND $logins = 0;
		$orm_obj->set('logins', $logins);
		$last_login = Arr::get($_REQUEST, 'last_login');
		$last_login == '' AND $last_login = 0;
		$last_login_timestamp = Arr::get($_REQUEST, 'last_login-timestamp');
		$last_login_timestamp == '' AND $last_login_timestamp = 0;
		$orm_obj->set('last_login', $last_login_timestamp);
		$extra = Arr::get($_REQUEST, 'extra');
		$orm_obj->set('extra', $extra);
		
		$password = Arr::get($_REQUEST, 'password');
		if ($password != NULL)
		{
			$orm_obj->set('password', $password);
		}
		
		$orm_obj->save();
		
		if (Auth::instance()->logged_in('admin'))
		{
			$companies = $this->user->get('companies')->find_all();
		}
		
		foreach ($orm_obj->has_many() as $key=>$model_value) {
			if ($key != 'user_tokens')
			{
				$values = Arr::get($_REQUEST, $key, array());
				if (count($values) > 0) {
					foreach ($values as $value)
					{
						if ($value != '')
						{
							if(!$orm_obj->has($key, $value))
							{
								$orm_obj->add($key, $value);
							}
						}
					}
				}
				$all_many_objs = array();
				foreach (ORM::factory($model_value['model'])->find_all() as $many_obj)
				{
					$all_many_objs[] = $many_obj->pk();
				}
				$many_objs_to_remove = array_diff($all_many_objs, $values);
				if (count($many_objs_to_remove) > 0)
				{
					$orm_obj->remove($key, $many_objs_to_remove);
				}
			}
			if ($key == 'companies' AND isset($companies))
			{
				if (count($companies) > 0) {
					foreach ($companies as $company)
					{
						$value = $company->pk();
						if ($value != '')
						{
							if(!$orm_obj->has('companies', $value))
							{
								$orm_obj->add('companies', $value);
							}
						}
					}
				}
			}
		}
		
		$user_tokens = Arr::get($_REQUEST, 'user_tokens');
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	public function action_save_role() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('Role', $id);
		$name = Arr::get($_REQUEST, 'name');
		$orm_obj->set('name', $name);
		$description = Arr::get($_REQUEST, 'description');
		$orm_obj->set('description', $description);
		
		$orm_obj->save();
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	public function action_save_token() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		
		$id = Arr::get($_REQUEST, 'id');
		$orm_obj = ORM::factory('User_Token', $id);
		$user_id = Arr::get($_REQUEST, 'user_id');
		$orm_obj->set('user_id', $user_id);
		$user_agent = Arr::get($_REQUEST, 'user_agent');
		$orm_obj->set('user_agent', $user_agent);
		$token = Arr::get($_REQUEST, 'token');
		$orm_obj->set('token', $token);
		$created = Arr::get($_REQUEST, 'created');
		$created_timestamp = Arr::get($_REQUEST, 'created-timestamp');
		$orm_obj->set('created', $created_timestamp);
		$expires = Arr::get($_REQUEST, 'expires');
		$expires_timestamp = Arr::get($_REQUEST, 'expires-timestamp');
		$orm_obj->set('expires', $expires_timestamp);
		
		$orm_obj->save();
		
		$result->datatable_index = Arr::get($_REQUEST, 'datatable_index');
		$result->id = $orm_obj->pk();
		$result->row_data = $this->get_orm_obj_dt_row($orm_obj);
		$result->status = 'OK';
		echo json_encode($result);
	}
	
	protected function get_orm_obj_dt_row($orm_obj)
	{
		$row_data = array();
		$current_cell = 0;
		$list_columns = $orm_obj->list_columns();
		$table_columns = $orm_obj->table_columns();
		foreach ($list_columns as $column_name=>$column_info)
		{
			$extra_column_info = Arr::get($table_columns, $column_name);
			isset($extra_column_info['data_type']) AND $column_info['data_type'] = $extra_column_info['data_type'];
			isset($extra_column_info['type']) AND $column_info['type'] = $extra_column_info['type'];
			$column_info['list_view'] = Arr::get($extra_column_info, 'list_view', TRUE);
			$column_info['required'] = Arr::get($extra_column_info, 'required', NULL);
			$list_columns[$column_name] = $column_info;
		}
		foreach ($orm_obj->as_array() as $key=>$value) {
			$column_info = Arr::get($list_columns, $key);
			if ($column_info['list_view'])
			{
				if ($column_info['data_type'] == 'timestamp' AND $value != NULL) {
					$row_data[$current_cell++] = DateTime::createFromFormat('U',$value)->format('Y-m-d H:i:s');
				} else {
					$row_data[$current_cell++] = $value;
				}
			}
		}
		foreach ($orm_obj->has_many() as $key=>$value) {
			$first = TRUE;
			$row_data[$current_cell] = "";
			foreach ($orm_obj->get($key)->find_all() as $many_key=>$many_value)
			{
				if (!$first)
				{
					$row_data[$current_cell] .= "<br>";
				}
				$row_data[$current_cell] .=  Form::hidden($key.'[]', $many_value->pk());
				$row_data[$current_cell] .=  (array_key_exists('name',$many_value->as_array()) ? $many_value->get('name') : $many_value->pk());
				$first = FALSE;
			}
			$current_cell++;
		}
		return $row_data;
	}
	
	public function action_delete_orm_obj() {
		$this->ajax_data = 'json';
		$this->response->headers('Content-Type','application/json');
		$result = new StdClass;
		$id = Arr::get($_REQUEST, 'id');
		$orm_name = Arr::get($_REQUEST, 'orm_name');
		if($orm_name != NULL)
		{
			$orm_obj = ORM::factory($orm_name, $id);
			if ($orm_obj->loaded())
			{
				$orm_obj->delete();
				$result->status = 'OK';
			}else
			{
				$result->status = 'KO';
				$result->message = $orm_name.' not found.';
			}
		} else
		{
			$result->status = 'KO';
			$result->message = 'Class not found.';
		}
		echo json_encode($result);
	}
	
}
?>
