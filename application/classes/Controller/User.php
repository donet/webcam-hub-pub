<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template {

	public function action_info() {
		$user = Auth::instance()->get_user();
		$this->template->user = $user;
		$this->session = Session::instance();
		$this->template->title = "View User Information";
		$this->template->content = View::factory('user/info')
			->bind('user', $user);
	}
	
	public function action_create() {
		$this->template = View::factory('template_user');
		$this->template->title = "Create User";
		$this->template->content = View::factory('user/login')
			->bind('errors', $errors)
			->bind('message', $message);
		if (HTTP_Request::POST == $this->request->method()) {
			// validate
			$post = Validation::factory($_POST)
			->rule('email', 'not_empty')
			->rule('email', 'email')
			->rule('email', 'email_domain')
			->rule('username', 'not_empty')
			->rule('username', 'min_length', array(':value', Kohana::$config->load('app.user.create.username.min_length')))
			->rule('username', 'max_length', array(':value', Kohana::$config->load('app.user.create.username.max_length')))
			->rule('password', 'not_empty')
			->rule('password', 'min_length', array(':value', Kohana::$config->load('app.user.create.password.min_length')))
			->rule('password', array($this, 'pwdneusr'), array(':validation', ':field', 'username'));
			if ($post->check()) {
				try {
					// Create the user using form values
					$user = ORM::factory('User')->create_user($this->request->post(), array(
						'username',
						'password',
						'email'
					));
					// Grant user login role
					$user->add('roles', ORM::factory('Role', array('name' => 'login')));
					// Reset values so form is not sticky
					$_POST = array();
					// Set success message
					$message = "You have added user '{$user->username}' to the database";
				} catch (ORM_Validation_Exception $e) {
					// Set failure message
					$message = 'There were errors, please see form below.';
					// Set errors using custom messages
					$errors = $e->errors('models');
				}
			} else {
				$errors = $post->errors('models');
			}
		}
	}
	
	public function action_update() {
		$this->template = View::factory('template_user');
		$this->template->title = "Update User Information";
		$this->template->content = View::factory('user/update')
			->bind('user', $user)
			->bind('message', $message)
			->bind('errors', $errors);

		$user = Auth::instance()->get_user();

		if ($this->request->method() == HTTP_Request::POST) {
			try {
				$user->update_user($this->request->post(), array(
				'username', 'password', 'email',
				));
				$message = "Account updated successfully.";
			} catch (ORM_Validation_Exception $e) {
				$errors = $e->errors('controllers');
			}
		}
	}
	
	public function action_forgotpassword() {
		$this->template = View::factory('template_user');
		$this->template->title = "Reset Forgotten Password";
		$this->template->content = View::factory('user/login')
			->bind('message', $message);

		if ($this->request->method() == HTTP_Request::POST) {
			$user = ORM::factory('User')
				->where('email', '=', $this->request->post('email'))
				->find();

			if ($user->loaded()) {
				$password_reset = $this->reset_password($user);
				if ($password_reset === TRUE) {
					$message = "An email containing your new password has been sent.";
				} else {
					$errors = $password_reset;
				}
			} else {
				$message = "User not found.";
			}
		}
	}

	public function reset_password(Model_User $user) {
		// received the POST
		if (isset($_POST) && Valid::not_empty($_POST)) {		
			// validate the login form
			$post = Validation::factory($_POST)
			->rule('email', 'not_empty')
			->rule('email', 'email')
			->rule('email', 'email_domain')
			->rule('email', array($this, 'emailexist'), array(':validation', ':field')); //TODO duplicate of ORM::factory('user') below
				
			// if the form is valid and the username and password matches
			if ($post->check()) {
				try {
					$reset_token = $this->generate_token(32);
					$user->setExtra('reset_token', $reset_token);
					$user->save();

					$msg_body = View::factory('email/user');
					$msg_body->msg_title = __("Hi :username you have requested a password reset.", array(
						':username' => $user->username
					));
					$msg_body->msg_content = __("You can reset password to your account by visiting the page at:");
					$msg_body->msg_button_text = __('Reset Password');
					$msg_body->msg_button_link = URL::site('user/reset?token='.$reset_token.'&email='.$_POST['email'], TRUE);
					$envelope_from = 'order@skiperformance.com';
					$noreply = TRUE;
					$eol = PHP_EOL; // "\r\n"
					$headers  = 'MIME-Version: 1.0' . $eol;
					$headers .= 'From: '. $envelope_from . $eol;
					$headers .= 'Cc: '. '' . $eol;
					$headers .= 'Bcc: '. '' . $eol;
					$headers .= $noreply ? 'Reply-To: noreply@'.'skiperformance.com' . $eol:'';
					$headers .= 'Content-type: text/html; charset=UTF-8' . $eol;
					if (!mail($user->email, "Password Reset", $msg_body, $headers, '-f '.$envelope_from)) {
						$errors['email'] = "Could not send email.";
					}
				} catch (ORM_Validation_Exception $e) {
					$errors = $e->errors('controllers');
				}
			}
		}
		// display
		/*$this->template->title = 'Reset password';
		$this->template->content = View::factory('account/reset')
			->bind('post', $post)
			->bind('errors', $errors)
			->bind('sent', $sent);*/
		return (isset($errors)) ? $errors : TRUE;
	}
	
	public function action_reset() {
		$this->template = View::factory('template_user');
		$this->template->title = "Create User";
		$this->template->content = View::factory('user/reset_pw')
			->bind('errors', $errors)
			->bind('message', $message);
		if (HTTP_Request::POST == $this->request->method()) {
			$user = ORM::factory('User')
				->where('email', '=', $this->request->post('email'))
				->find();

			if ($user->loaded()) {
				if ($user->getExtra('reset_token') != NULL AND $user->getExtra('reset_token') == $this->request->post('token')) {
					// validate
					$post = Validation::factory(array_merge($_POST, array('username'=>$user->get('username'))))
					->rule('email', 'not_empty')
					->rule('email', 'email')
					->rule('email', 'email_domain')
					->rule('password', 'not_empty')
					->rule('password', 'min_length', array(':value', Kohana::$config->load('app.user.create.password.min_length')))
					->rule('password', array($this, 'pwdneusr'), array(':validation', ':field', 'username'));
					if ($post->check()) {
						try {
							$user->set('password', $this->request->post('password'));
							$user->setExtra('reset_token', NULL);
							$user->save();
							// display
							$this->template->title = 'Reset password';
							$this->template->content = View::factory('user/login')
								->bind('post', $post)
								->bind('errors', $errors)
								->bind('message', $message);
							$message = "Password save correctly, now you can login with new password.";
						} catch (ORM_Validation_Exception $e) {
							// Set failure message
							$message = 'There were errors, please see form below.';
							// Set errors using custom messages
							$errors = $e->errors('models');
						}
					} else {
						$message = 'There were errors, please see form below.';
						$errors = $post->errors('models');
					}
				} else {
					$message = "Token incorrect.";
				}
			} else {
				$message = "User not found.";
			}
		}
	}

	public function action_login() {
		$this->template = View::factory('template_user');
		$this->template->title = "Login";
		$this->template->content = View::factory('user/login')
			->bind('message', $message);
		if (HTTP_Request::POST == $this->request->method()) {
			// Attempt to login user
			$remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
			$user = Auth::instance()->login($this->request->post('username'), $this->request->post('password'), $remember);
			// If successful, redirect user
			if ($user || Auth::instance()->auto_login()) {
				if (Auth::instance()->logged_in()) {
					$this->session = Session::instance();
					if ($this->session->get('page_wanted') != NULL) {
						$this->redirect($this->session->get('page_wanted'));
						$this->session->delete('page_wanted');
					} else {
						$this->redirect(Route::url('default',NULL, TRUE));
					}
				} else {
					$message = 'Permiss Deined';
				}
			} else {
				$message = 'Login failed';
			}
		}
	}
	
	public function action_logout() {
		// Log user out
		Auth::instance()->logout(TRUE, TRUE);
		// Redirect to login page
		$this->redirect('user/login');
	}

	// validation rule: password != username
	static function pwdneusr($validation, $password, $username)
	{
		if ($validation[$password] === $validation[$username])
		{
			$validation->error($password, 'pwdneusr');
		}
	}
	//validation rule: email exist
	static function emailexist($validation, $email)
	{
		if(!ORM::factory('user')->unique_key_exists($validation[$email])) {
			$validation->error($email, 'emailexistnot');
		}
	}
	// generate token
	static function generate_token($length = 8) {
		// start with a blank password
		$password = "";
		// define possible characters (does not include l, number relatively likely)
		$possible = "123456789abcdefghjkmnpqrstuvwxyz123456789";
		// add random characters to $password until $length is reached
		for ($i = 0; $i < $length; $i++) {
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$password .= $char;
		}
		return $password;
	}
}
?>
