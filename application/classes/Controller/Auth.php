<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Template {

	protected $user;

	public function __construct(Request $request, Response $response) {
		parent::__construct($request, $response);
	}
	
	public function before() {
		parent::before();
		$this->template->with_sidebar = TRUE;
		$this->session = Session::instance();
		if (Auth::instance()->logged_in()) {
			$page_wanted = $this->session->get('page_wanted');
			if ($page_wanted != NULL) {
				$this->session->delete('page_wanted');
				$this->redirect($page_wanted);
			}
			$this->user = Auth::instance()->get_user();
			$this->template->user = $this->user;
		} else {
			// User isn't logged in.
			//try to auto-login
			if (Auth::instance()->auto_login() != FALSE) {
				//all right, logged in
				$page_wanted = $this->session->get('page_wanted');
				if ($page_wanted != NULL) {
					$this->session->delete('page_wanted');
					$this->redirect($page_wanted);
				}
				$this->user = Auth::instance()->get_user();
				$this->template->user = $this->user;
			} else {
				// User isn't logged in.
				// Save page_wanted
				$this->session->set('page_wanted', $this->request->uri());
				// Redirect to the login form.
				$this->redirect(Route::url('default', array(
					'controller' => 'User',
					'action' => 'login',
				),TRUE));
			}
		}
	}
	
	public function after() {
		if (!isset($this->ajax_data) OR (isset($this->ajax_data) AND $this->ajax_data == 'html'))
		{
			if (is_object($this->template->content))
			{
				$this->template->content->user = $this->user;
				$this->template->modals->user = $this->user;
			}
			parent::after();
		}
	}

}
?>
