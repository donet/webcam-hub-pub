<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend extends Controller {

	public function action_script_data()
	{
		$player = new StdClass();
		$cache = Cache::instance();
		$cache_key = $this->request->controller().'_'.$this->request->action().'_'.md5(serialize($this->request->param()).serialize($_REQUEST));
		$cached_response = $cache->get($cache_key);
		if ($cached_response != NULL)
		{
			$this->response->headers('Content-Type', $cached_response->headers);
			$this->response->body($cached_response->body);
			return;
		}
		
		$obj_id = $this->request->param('id');
		$obj_name = 'Player';
		$obj = ORM::factory($obj_name, $obj_id);
		if ($obj_id == NULL OR !$obj->loaded())
		{
			$player = 'Error';
			return;
		}
		$view = View::factory('frontend/script_data');
		$view->player = $obj->getAjaxData();
		$view->obj_name = $obj_name;
		$this->response->body($view);
		$cached_value = new StdClass;
		$cached_value->headers = $this->response->headers();
		$cached_value->body = $this->response->body();
		$cache->set($cache_key, $cached_value);
	}
	
	public function action_webcam()
	{
		
		$webcam = ORM::factory('Webcam', Arr::get($_REQUEST, 'webcam_id'));
		if ($webcam->loaded())
		{
			$view = View::factory('frontend/webcam');
			$view->webcam_url = $webcam->get('url');
			$view->webcamViewer_settings = $webcam->getWebcamViewerSettings();
			$this->response->body($view);
		}
		
	}

} // End Frontend
