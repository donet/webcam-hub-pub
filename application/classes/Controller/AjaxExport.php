<?php defined('SYSPATH') or die('No direct script access.');

class Controller_AjaxExport extends Controller {

	protected $json_payload;

	public function before()
	{
		$this->json_payload = new StdClass();
		parent::before();
	}

	public function after()
	{
		parent::after();
	}

	public function action_feed()
	{
		$cache = Cache::instance();
		$obj_id = Arr::get($_REQUEST, 'obj_id');
		$obj_name = Arr::get($_REQUEST, 'obj_name');
		$obj = ORM::factory($obj_name, $obj_id);
		if ($obj_id == NULL OR !$obj->loaded())
		{
			$this->json_payload = 'Error';
			return;
		}
		$this->json_payload->$obj_name = $obj->getAjaxData();
		$this->response->headers('Content-Type', 'application/json');
		$this->response->body(json_encode($this->json_payload));
	}
	
	public function action_blob($is_image = FALSE)
	{
		$cache = Cache::instance();
		$cache_key = $this->request->controller().'_'.$this->request->action().'_'.md5(serialize($this->request->param()).serialize($_REQUEST));
		$cached_response = $cache->get($cache_key);
		if ($cached_response != NULL)
		{
			$this->response->headers('Content-Type', $cached_response->headers);
			$this->response->body($cached_response->body);
			return;
		}
		
		$obj_name = $this->request->param('obj');
		$field = $this->request->param('field');
		$obj_id = $this->request->param('id');
		
		$obj = ORM::factory($obj_name, $obj_id);
		if ($obj->loaded() == FALSE)
		{
			throw new HTTP_Exception_404();
		}else{
			$value = $obj->get($field);
			$cached_value = new StdClass;
			if ($is_image)
			{
				$params['new_img_type'] = Arr::get($_REQUEST,'image_type');
				
				$params['width'] = Arr::get($_REQUEST,'width');
				$params['height'] = Arr::get($_REQUEST,'height');
				
				$new_img = $obj->modifyImage($params);
				$cached_value->headers = $obj::getResponseHeader($new_img->image_type);
				$value = $new_img->value;
				
			}else{
				$cached_value->headers = $obj::getResponseHeader($obj->getFileExtension());
			}
			$cached_value->body = Arr::get($_REQUEST, 'base64') != NULL ? base64_encode($value): $value;
			$this->response->headers('Content-Type', $cached_value->headers);
			$this->response->body($cached_value->body);

			$cache->set($cache_key, $cached_value);
		}
	}

	public function action_image()
	{
		return $this->action_blob(TRUE);
	}

}
