<?php defined('SYSPATH') or die('No direct script access.');

class Utils {
	// Utils::getURLinVersion($url);
	public static function getURLinVersion($request = NULL, $url)
	{
		return URL::base($request, TRUE).$url.'?nc='.filemtime(DOCROOT.$url);
	}
}
