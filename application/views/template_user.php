<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title><?php echo __('project_title');?> | <?php echo $title; ?></title>
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?=Utils::getURLinVersion($_REQUEST, "media/images/wequid-icon.png")?>" type="image/png">
		<!-- APPCONS -->
		<link rel="apple-touch-icon" href="<?=Utils::getURLinVersion($_REQUEST, "media/images/wequid-icon.png")?>">

		<!-- Bootstrap -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/bootstrap/dist/css/bootstrap.min.css");?>" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet">
		<!-- NProgress -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/nprogress/nprogress.css");?>" rel="stylesheet">
		<!-- iCheck -->
		<link href=<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/iCheck/skins/flat/green.css");?>" rel="stylesheet">
		<!-- Animate.css -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/animate.css/animate.min.css");?>" rel="stylesheet">

		<!-- Custom Theme Style -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/css/template.css");?>" rel="stylesheet">
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/css/custom.css");?>" rel="stylesheet">
	</head>
	<body>
		<?= $content; ?>
		
		<div id="privacy_terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title" id="myModalLabel"><?php echo __('privacy_terms');?></h4>
					</div>
					<div class="modal-body">
						<?php echo __('privacy_terms.html_text');?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- LOADER -->
		<div id="js-loader-template" class="loader js-loader">
			<svg class="loader-spinner" version="1.1" xmlns="http://www.w3.org/2000/svg" width="120" height="120" style="opacity: 1;">
				<defs>
					<clipPath id="clipPath">
						<circle cx="60" cy="60" r="55"></circle>
					</clipPath>
				</defs>
				<g class="loader-background" transform="translate(0,0) rotate(89.99532000627369,60,60) skewX(0) scale(1,1)">
					<g transform="translate(120, 0) scale(-1, 1)">
						<circle class="loader-line" cx="60" cy="60" r="50" clip-path="url(#clipPath)" style="stroke-dasharray: 120, 37; stroke-width: 10; stroke: rgb(0, 131, 255);"></circle>
						<g transform="translate(95, 45)">
							<path class="loader-arrow" d="M1 16L14.2 0l13.2 15.8H1z" transform="translate(0,0) rotate(0,0,0) skewX(0) scale(1,1)" style="fill: rgb(0, 131, 255); opacity: 1;"></path>
						</g>
						<g transform="translate(25, 75) rotate(180)">
							<path class="loader-arrow" d="M1 16L14.2 0l13.2 15.8H1z" transform="translate(0,0) rotate(0,0,0) skewX(0) scale(1,1)" style="fill: rgb(0, 131, 255); opacity: 1;"></path>
						</g>
					</g>
				</g>
				<g class="loader-tick" transform="translate(0,0) rotate(-45,60,60) skewX(0) scale(1,1)" style="opacity: 0;">
					<path fill="white" d="M17.977 35.553L0 16.276l5.112-4.768 17.727 19.01L52.31 0l4.93 4.76L22.83 40.39l-.316-.305-.168.156-2.98-3.194-1.465-1.416.074-.077z" transform="translate(28, 40) scale(1.15)" fill-rule="evenodd"></path>
				</g>
				<g class="loader-wrong" transform="translate(0,0) rotate(-45,60,60) skewX(0) scale(1,1)" style="opacity: 0;">
					<path fill="white" d="M27,29H17V0h10V29z M27,44H17v-8h10V44z" transform="translate(35, 35) scale(1.15)" fill-rule="evenodd" />
				</g>
			</svg>
		</div>
		<!-- /LOADER -->
		
		<!-- jQuery -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/jquery/dist/jquery.min.js");?>"></script>
		<!-- Bootstrap -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/bootstrap/dist/js/bootstrap.min.js");?>"></script>
		<!-- FastClick -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/fastclick/lib/fastclick.js");?>"></script>
		<!-- NProgress -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/nprogress/nprogress.js");?>"></script>
		<!-- iCheck -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/iCheck/icheck.min.js");?>"></script>
		<!-- validator -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/validator/validator.js");?>"></script>
		<!-- Loader Scripts -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/js/dynamics.js");?>"></script>
		<!-- Custom Theme Scripts -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/js/template.js");?>"></script>
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/js/custom.js");?>"></script>
	</body>
</html>
