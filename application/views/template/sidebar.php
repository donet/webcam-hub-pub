<div class="col-md-3 left_col">
	<div class="left_col scroll-view">

		<div class="navbar nav_title" style="border: 0;">
			<a href="<?php echo Route::url('default'); ?>" class="site_title"><span><?php echo __('project_title');?></span></a>
		</div>

		<div class="clearfix"></div></br>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<!-- menu -->
			<div class="menu_section">
				<h3><?=__('Webcam Hub');?></h3>
				<?php if (count(Auth::instance()->get_user()->getRoles()) > 1) { ?>
				<ul class="nav side-menu">
					<li><a href="<?php echo Route::url('default', array('controller' => 'WebcamHubManager', 'action' => 'webcamList')); ?>"><i class="fa fa-edit"></i> <?=__('Webcam');?> </a></li>
					<li><a href="<?php echo Route::url('default', array('controller' => 'WebcamHubManager', 'action' => 'playerList')); ?>"><i class="fa fa-edit"></i> <?=__('Player');?> </a></li>
					<li><a href="<?php echo Route::url('default', array('controller' => 'WebcamHubManager', 'action' => 'colorList')); ?>"><i class="fa fa-edit"></i> <?=__('Color');?> </a></li>
					<?php if (Auth::instance()->logged_in('superadmin')) { ?>
					<li><a href="<?php echo Route::url('default', array('controller' => 'WebcamHubManager', 'action' => 'companyList')); ?>"><i class="fa fa-edit"></i> <?=__('Company');?> </a></li>
					<?php } ?>
				</ul>
				<?php } ?>
			</div>
			<!-- /menu -->
			<!-- admin menu -->
			<?php if (Auth::instance()->logged_in('admin') OR Auth::instance()->logged_in('superadmin')) { ?>
			<div class="menu_section">
				<h3><?=__('Admin Area');?></h3>
				<ul class="nav side-menu">
					<li><a><i class="fa fa-user"></i> User Manager <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo Route::url('default', array('controller' => 'UserManager', 'action' => 'userList')); ?>">User List</a></li>
							<?php if (Auth::instance()->logged_in('superadmin')) { ?>
							<li><a href="<?php echo Route::url('default', array('controller' => 'UserManager', 'action' => 'tokenList')); ?>">Token List</a></li>
							<li><a href="<?php echo Route::url('default', array('controller' => 'UserManager', 'action' => 'roleList')); ?>">Roles List</a></li>
							<?php } ?>
						</ul>
					</li>
				</ul>
			</div>
			<?php } ?>
			<!-- /admin menu -->
		</div>
		<!-- /sidebar menu -->

		<!-- /menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo Route::url('default', array('controller' => 'User', 'action' => 'logout')); ?>">
				<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->

	</div>
</div>
