<!-- top navigation -->
<div class="top_nav <?php if (!$with_sidebar) { echo 'no_sidebar'; } ?>">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle <?php if (!$with_sidebar) { echo 'no_sidebar'; } ?>">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="<?=Utils::getURLinVersion($_REQUEST ,"media/images/user-his.png");?>" alt=""><?=$user->username; ?>
						<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="<?php echo Route::url('default', array('controller' => 'User', 'action' => 'info')); ?>"> Profile</a></li>
						<li><a href="<?php echo Route::url('default', array('controller' => 'User', 'action' => 'logout')); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>
<!-- /top navigation -->
