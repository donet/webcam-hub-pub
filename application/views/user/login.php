<?php if (isset($errors)) { ?>
	<script>
		window.location.hash = 'signup';
	</script>
<?php } ?>

<div class="login">
	<a class="hiddenanchor" id="signup"></a>
	<a class="hiddenanchor" id="signin"></a>
	<a class="hiddenanchor" id="recoverpwd"></a>

	<div class="login_wrapper">

		<div class="animate form login_form">
			<section class="login_content">
				<?= Form::open('user/login'); ?>
					<h1>Login Form</h1>
					<?php if ($message > '') { ?>
					<div class="alert alert-info alert-dismissible fade in text-left" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?=$message; ?>
					</div>
					<?php } ?>
					<div class="col-md-12">
					  <ul class="social-network social-circle">
							<!--li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li-->
							<!--li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li-->
							<!--li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li-->
							<!--li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li-->
							<!--li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li-->
					  </ul>				
					</div>
					<div>
						<input type="text" class="form-control" name="username" value="<?=HTML::chars(Arr::get($_POST, 'username')); ?>" placeholder="Username" required="required" />
					</div>
					<div>
						<input type="password" class="form-control" name="password" value="<?=HTML::chars(Arr::get($_POST, 'password')); ?>" placeholder="Password" required="required" />
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" class="flat" name="remember"> Remenber
						</label>
					</div>
					<div>
						<button type="submit" class="btn btn-default submit" href="index.html">Log in</button>
						<a class="reset_pass" href="#recoverpwd">Lost your password?</a>
					</div>
					<div class="clearfix"></div>
					<div class="separator">
						<p class="change_link">New to site?
							<a href="#signup" class="to_register"> Create Account </a>
						</p>
						<div class="clearfix"></div>
					</div>
					<h1><?php echo __('project_title');?></h1>
					<p>©<?php echo date("Y");?> <?php echo __('project_copyright');?> <a href="#privacy_terms" data-toggle="modal"><?php echo __('privacy_terms');?></a></p>
				</form>
			</section>
		</div><!-- .login_form -->

		<div id="register" class="animate form registration_form">
			<section class="login_content">
				<?= Form::open('user/create'); ?>
					<h1>Create Account</h1>
					<?php if ($message > '') { ?>
					<div class="alert alert-info alert-dismissible fade in text-left" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?=$message; ?>
					</div>
					<?php } ?>
					<div class="col-md-12">
					  <ul class="social-network social-circle">
							<!--li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li-->
							<!--li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li-->
							<!--li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li-->
							<!--li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li-->
							<!--li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li-->
					  </ul>				
					</div>
					<div>
						<input type="text" class="form-control" name="username" value="<?=HTML::chars(Arr::get($_POST, 'username')); ?>" placeholder="Username" required="required" />
						<?php if (isset($errors) AND Arr::get($errors, 'username') > '') { ?>
						<div class="alert alert-danger alert-dismissible fade in text-left" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<?= Arr::get($errors, 'username'); ?>
						</div>
						<?php } ?>
					</div>
					<div>
						<input type="email" class="form-control" name="email" value="<?=HTML::chars(Arr::get($_POST, 'email')); ?>" placeholder="Email" required="required" />
						<?php if (isset($errors) AND Arr::get($errors, 'email') > '') { ?>
						<div class="alert alert-danger alert-dismissible fade in text-left" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<?= Arr::get($errors, 'email'); ?>
						</div>
						<?php } ?>
					</div>
					<div>
						<input type="password" class="form-control" name="password" value="<?=HTML::chars(Arr::get($_POST, 'password')); ?>" placeholder="Password" required="required" />
						<?php 
						if (isset($errors)) {
							$password_error = Arr::get($errors, 'password') > '' ? Arr::get($errors, 'password') : Arr::path($errors, '_external.password');
							$password_confirm_error = Arr::get($errors, 'password_confirm') > '' ? Arr::get($errors, 'password_confirm') : Arr::path($errors, '_external.password_confirm');
							if ($password_error > '' OR $password_confirm_error > '') { ?>
							<div class="alert alert-danger alert-dismissible fade in text-left" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
								</button>
								<?= $password_error.'</br>'.$password_confirm_error; ?>
							</div>
							<?php }
							} ?>
					</div>
					<div>
						<button type="submit" class="btn btn-default submit" href="index.html">Submit</button>
					</div>
					<div class="clearfix"></div>
					<div class="separator">
						<p class="change_link">Already a member ?
							<a href="#signin" class="to_register"> Log in </a>
						</p>
						<div class="clearfix"></div>
					</div>
					<h1><?php echo __('project_title');?></h1>
					<p>©<?php echo date("Y");?> <?php echo __('project_copyright');?> <a href="#privacy_terms" data-toggle="modal"><?php echo __('privacy_terms');?></a></p>
				</form>
			</section>
		</div><!-- #register -->

		<div id="forget" class="animate form forget_form">
			<section class="login_content">
				<?= Form::open('user/forgotpassword'); ?>
					<h1>Recover Form</h1>
					<?php if ($message > '') { ?>
					<div class="alert alert-info alert-dismissible fade in text-left" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?=$message; ?>
					</div>
					<?php } ?>
					<div>
						<input type="email" class="form-control" name="email" value="<?=HTML::chars(Arr::get($_POST, 'email')); ?>" placeholder="Email" required="required" />
					</div>
					<div>
						<button type="submit" class="btn btn-default submit" href="index.html">Recover Password</button>
					</div>
					<div class="clearfix"></div>
					<div class="separator">
						<p class="change_link">Already a member ?
							<a href="#signin" class="to_forget"> Log in </a>
						</p>
						<p class="change_link">New to site?
							<a href="#signup" class="to_forget"> Create Account </a>
						</p>
						<div class="clearfix"></div>
					</div>
					<h1>111<?php echo __('project_title');?>222</h1>
					<p>©<?php echo date("Y");?> <?php echo __('project_copyright');?> <a href="#privacy_terms" data-toggle="modal"><?php echo __('privacy_terms');?></a></p>
				</form>
			</section>
		</div><!-- #forget -->

	</div><!-- .login_wrapper -->
</div><!-- .login -->
