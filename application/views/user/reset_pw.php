<div class="login">

	<div class="login_wrapper">

		<div id="register" class="form">
			<section class="login_content">
				<?= Form::open('user/reset');?>
					<input type="hidden" class="form-control" name="token" value="<?=HTML::chars(Arr::get($_REQUEST, 'token')); ?>" />
					<h1>Reset Password</h1>
					<?php if ($message > '') { ?>
					<div class="alert alert-info alert-dismissible fade in text-left" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?=$message; ?>
					</div>
					<?php } ?>
					<div>
						<input type="email" class="form-control" name="email" value="<?=HTML::chars(Arr::get($_REQUEST, 'email')); ?>" placeholder="Email" required="required" readonly="readonly" />
						<?php if (isset($errors) AND Arr::get($errors, 'email') > '') { ?>
						<div class="alert alert-danger alert-dismissible fade in text-left" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<?= Arr::get($errors, 'email'); ?>
						</div>
						<?php } ?>
					</div>
					<div>
						<input type="password" class="form-control" name="password" value="<?=HTML::chars(Arr::get($_POST, 'password')); ?>" placeholder="Password" required="required" />
						<?php 
						if (isset($errors)) {
							$password_error = Arr::get($errors, 'password') > '' ? Arr::get($errors, 'password') : Arr::path($errors, '_external.password');
							$password_confirm_error = Arr::get($errors, 'password_confirm') > '' ? Arr::get($errors, 'password_confirm') : Arr::path($errors, '_external.password_confirm');
							if ($password_error > '' OR $password_confirm_error > '') { ?>
							<div class="alert alert-danger alert-dismissible fade in text-left" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
								</button>
								<?= $password_error.'</br>'.$password_confirm_error; ?>
							</div>
							<?php }
							} ?>
					</div>
					<div>
						<button type="submit" class="btn btn-default submit" href="index.html">Submit</button>
					</div>
					<div class="clearfix"></div>
					<h1><?php echo __('project_title');?></h1>
					<p>©<?php echo date("Y");?> <?php echo __('project_copyright');?> <a href="#privacy_terms" data-toggle="modal"><?php echo __('privacy_terms');?></a></p>
				</form>
			</section>
		</div><!-- #register -->

	</div><!-- .login_wrapper -->
</div><!-- .login -->
