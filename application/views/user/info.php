<h2>Info for  user "<?= $user->username; ?>"</h2>

<ul>
	<li>Email: <?= $user->email; ?></li>
	<li>Number of logins: <?= $user->logins; ?></li>
	<li>Last Login: <?= Date::fuzzy_span($user->last_login); ?></li>
	<li>Last Login: <?= DateTime::createFromFormat('U',$user->last_login)->format('Y-m-d H:i:s'); ?></li>
</ul>

<?= HTML::anchor('user/logout', 'Logout<i class="fa fa-sign-out pull-right"></i>', array('class'=>'btn btn-default')); ?>
