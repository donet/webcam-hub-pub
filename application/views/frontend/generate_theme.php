// These are the default colors

// colors
<?php foreach ($extra_list as $extra_name => $extra_info)
{
	echo '$' . $extra_name . ': ' . Arr::get($extra, $extra_name, Arr::get($extra_info, 'default_value', '#000000')) . ';' . "\n";
} ?>

@import "../shared/style";
@import "../shared/custom";

