var WQA = WQA || {};
WQA.webcams = <?=json_encode($player['webcams']);?>;
WQA.slideshowInterval = <?= $player['slideshowInterval'];?>;
WQA.slideshow_autoStart = <?=json_encode($player['slideshow_autoStart']);?>;
WQA.selected_key = <?= $player['selected_key'];?>;
WQA.icon_size = <?= $player['icon_size'];?>;
WQA.page_refresh = <?= $player['page_refresh'];?>;
WQA.skirama = '<?= (isset($player['skirama']) ? $player['skirama'] : '');?>';
WQA.theme = '<?= $player['theme'];?>';
