<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Webcam CSS -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/css/webcam.css");?>"  rel="stylesheet" type="text/css" />
		<!-- ImageViewer CSS -->
		<link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/ImageViewer/imageviewer.css");?>"  rel="stylesheet" type="text/css" />
		<!-- jQuery -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/jquery/dist/jquery.min.js");?>"></script>
		<!-- jQuery UI -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/jquery-ui-1.12.1/jquery-ui.js");?>"></script>
		<!-- ImageViewer JS -->
		<script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/ImageViewer/imageviewer.js");?>"></script>
		<!-- Webcam JS -->
		<script id="js-webcam-hub-starter-script" src="<?=Utils::getURLinVersion($_REQUEST ,"media/js/webcam.js");?>"></script>
	</head>
	<body>
		<img id="" class="webcam_viewer-img js-webcam_viewer-img" src="<?=$webcam_url?>" data-high-res-src="<?=$webcam_url?>" alt="">
		<script>
			$( document ).ready(function() {
				$('.js-webcam_viewer-img').WebcamViewer(<?=json_encode($webcamViewer_settings);?>);
				//$('.js-webcam_viewer-img').WebcamViewer({wheelScroll: false, wheelZoom: true});
				//$('.js-webcam_viewer-img').ImageViewer();
			});
		</script>
	</body>
</html>
