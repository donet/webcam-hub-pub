<div id="js-modal-code_generate-player_<?=$player_id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="js-modal-code_generate-player_<?=$player_id;?>-title">Code for Player <?=$player_id;?></h4>
			</div>
			<div class="modal-body">

<xmp style="display:block;width:100%;overflow: auto;padding: 0px;margin: 0px;"><iframe id="test-frame" src="<?=$base_url;?>fe/guest.php?player_id=<?=$player_id;?>" width="100%" height="100%" frameBorder="0" scrolling="no"></iframe>
<script id="js-webcam-hub-starter-script">
	js = document.createElement('script');
	m = document.querySelector('script#js-webcam-hub-starter-script');
	js.src = '<?=$base_url;?>fe/js/script-host.js?nc='+Date.now();
		js.type = 'text/javascript';
		js.id = 'js-webcam-hub-host-script';
		js.setAttribute('data-iframe_id', 'test-frame');
	m.parentNode.insertBefore(js, m);
</script>

</xmp>

			</div>
		</div>
	</div>
</div>
