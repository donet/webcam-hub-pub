<?php if ($show_actions['edit'] == TRUE) { ?>
<div id="<?=$dataTable_name;?>-Edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="dataTable-Edit-title"></h4>
			</div>
			<div class="modal-body">
				<form id="<?=$dataTable_name;?>-Edit-form" class="form-horizontal form-label-left">
					<?=Form::hidden('datatable_index', '');?>
					<?php foreach ($list_columns as $column_name=>$column_info) {
					if($column_info['edit_view'] AND $column_name != 'extra') {
						if ($column_info['type'] == 'int' AND strpos($column_info['data_type'], 'int') !== FALSE AND $column_info['extra'] == 'auto_increment') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="number" name="<?=$column_name;?>" id="<?=$column_name;?>" min="0" step="1" max="" pattern="\d" readonly class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'int' AND $column_info['data_type'] == 'tinyint' AND $column_info['display'] == '1') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<label>
									<input type="checkbox" name="<?=$column_name;?>" id="<?=$column_name;?>" class="js-switch" />
								</label>
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'int' AND strpos($column_info['data_type'], 'int') !== FALSE) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="number" name="<?=$column_name;?>" id="<?=$column_name;?>" min="0" step="1" max="" pattern="\d" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					<?php }
						}
					}
					$skirama = $orm_objs[0]->get('player')->get('skirama');
					if ($skirama != NULL) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Skirama
							</label>
							<div class="col-md-9 col-sm-9 col-xs-12" style="position:relative;">
								<img src="data:image/gif;base64,<?=base64_encode($skirama);?>" class="js-player-skirama" style="max-width:100%;width:100%;cursor:crosshair;" />
								<div class="player-skirama-pin js-pin">
								</div>
							</div>
						</div>
						<script>
							$('.js-player-skirama').on('click', function(e) {
								var offset = jQuery(e.currentTarget).offset();
								naturalSizeFactor = [e.currentTarget.naturalWidth/e.currentTarget.width, e.currentTarget.naturalHeight/e.currentTarget.height];
								x = (e.pageX - offset.left)*naturalSizeFactor[0];
								y = (e.pageY - offset.top)*naturalSizeFactor[1];
								$('#position_x').val(parseInt(x));
								$('#position_y').val(parseInt(y));
								skirama_bg = $('.js-player-skirama')[0];
								naturalSizeFactor = [100/skirama_bg.naturalWidth, 100/skirama_bg.naturalHeight];
								base_size = 15;
								$('.js-pin').attr('style', 'top:'+y*naturalSizeFactor[1]+'%;left:'+x*naturalSizeFactor[0]+'%;width:'+(base_size)+'px;height:'+(base_size)+'px;');
							});
						</script>
					<?php } ?>
				</form>
			</div>
			<div class="modal-footer">
				<?php $column_info = Arr::get($list_columns, 'extra');
				if ($column_info != NULL AND $column_info['edit_view']) { ?>
				<button class="btn btn-success btn-xs js-objExtra-Add"><i class="fa fa-plus"></i> <?=__('Add Extra');?> </button>
				<?php } ?>
				<button class="btn btn-default" onclick="close_modal('#<?=$dataTable_name;?>-Edit');">Close</button>
				<button type="button" form="<?=$dataTable_name;?>-Edit-form" class="btn btn-primary js-<?=$dataTable_name;?>-Edit-form-submit">Save</button>
			</div>
		</div>
	</div>
</div>

<div id="<?=$dataTable_name;?>-Edit-ajax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="dataTable-Edit-title"></h4>
			</div>
			<div class="modal-body" style="min-height:120px;">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<script>

$(document).ready(function() {
	<?php if ($show_actions['edit'] == TRUE) { ?>
	// Apply the edit
	$('#<?=$dataTable_name;?>').on('click', 'tbody tr td a.js-<?=$dataTable_name;?>-Edit', function (event) {
		loader_hide_in();
		row_datas = $(this).closest('td').siblings();
		$('#<?=$dataTable_name;?>-Edit-form input[name="datatable_index"]:not([novaluefromdt])').val(<?=$dataTable_name;?>.row( $(this).closest('tr') ).index());
		$(this).closest('table').find('thead th').each(function(indx, elem) {
			var data_type = $(elem).data('db_type');
			var data_key = $(elem).data('key');
			var data_extra = $(elem).data('extra');
			var data_name = $(elem).data('name');
			if (typeof data_type != typeof undefined) {
				if (data_type.indexOf('int') !== -1) {
					if (data_key.indexOf('MUL') !== -1) {
						value = $(row_datas[indx]).html();
						$('#<?=$dataTable_name;?>-Edit-form select[name="'+data_name+'"]:not([novaluefromdt]) option').each(function (indx, elem) {
							$(elem).prop('selected', $(elem).val() == value);
						});
					} else if (data_name == 'id') {
						$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).html());
						regenerate_objExtra(parseInt($(row_datas[indx]).html()))
					} else {
						$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).html());
					}
				} else if (data_type == 'varchar') {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).html());
				} else if (data_type == 'text') {
					$('#<?=$dataTable_name;?>-Edit-form textarea[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).html());
				} else if (data_type == 'date') {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setStartDate($(row_datas[indx]).html());
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setEndDate($(row_datas[indx]).html());
				} else if (data_type == 'time') {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setStartDate($(row_datas[indx]).html());
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setEndDate($(row_datas[indx]).html());
				} else if (data_type == 'datetime') {
					picker = $('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker');
					picker.setStartDate($(row_datas[indx]).html());
					picker.setEndDate($(row_datas[indx]).html());
					picker.element.siblings().val(moment(picker.element.val()+'Z').format('X'))
				} else if (data_type == 'timestamp') {
					picker = $('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker');
					picker.setStartDate($(row_datas[indx]).html());
					picker.setEndDate($(row_datas[indx]).html());
					picker.element.siblings().val(moment(picker.element.val()+'Z').format('X'))
				} else if (data_type == 'tinyint') {
					changeSwitcheryState($('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])'), parseInt($(row_datas[indx]).html()) == 1);
				} else if (data_type == 'enum') {
					value = $(row_datas[indx]).html();
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').each(function(indx, elem) {
						if ($(elem).val() == value) {
							$(elem).iCheck('check');
						} else {
							$(elem).iCheck('uncheck');
						}
					})
				} else if (data_type == 'set') {
					value = $(row_datas[indx]).html();
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').each(function(indx, elem) {
						if (value.indexOf($(elem).val()) !== -1) {
							$(elem).iCheck('check');
						} else {
							$(elem).iCheck('uncheck');
						}
					})
				} else if (data_type.indexOf('blob') !== -1) {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').siblings().remove();
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').before($(row_datas[indx]).html());
				}
			} else
			{
				// attencion Action column has data_name undefined
				if (typeof data_name != typeof undefined) {
					// has_many elements
					var dataarray = [];
					$(row_datas[indx]).find('[name="'+data_name+'[]"]').each(function(){
						dataarray.push($(this).val());
					});
					if (dataarray.length > 0)
					{
						$('#<?=$dataTable_name;?>-Edit-form select[name="'+data_name+'[]"]:not([novaluefromdt])').val(dataarray);
					} else
					{
						$('#<?=$dataTable_name;?>-Edit-form select[name="'+data_name+'[]"]:not([novaluefromdt])').val('');
					}
				}
			}
		});
		
		x = parseInt($('#position_x').val());
		y = parseInt($('#position_y').val());
		skirama_bg = $('.js-player-skirama')[0];
		naturalSizeFactor = [100/skirama_bg.naturalWidth, 100/skirama_bg.naturalHeight];
		base_size = 15;
		$('.js-pin').attr('style', 'top:'+y*naturalSizeFactor[1]+'%;left:'+x*naturalSizeFactor[0]+'%;width:'+(base_size)+'px;height:'+(base_size)+'px;');
		
	});
	<?php } ?>

	<?php if ($show_actions['add'] == TRUE) { ?>
	// Add elem
	$("div.toolbar").html("<a href=\"#<?=$dataTable_name;?>-Edit\" data-toggle=\"modal\" class=\"btn btn-success btn-xs js-dataTables-Add\"><i class=\"fa fa-plus\"></i> Add </a>").css('display', 'inline-block');
	$('.js-dataTables-Add').on('click', function (event) {
		loader_hide_in();
		changeSwitcheryState($('#<?=$dataTable_name;?>-Edit-form input[type="checkbox"]'), false);
		$('#<?=$dataTable_name;?>-Edit-form input[type="checkbox"]').each(function (indx, elem) {
			$(elem).iCheck('uncheck');
		});
		$('#<?=$dataTable_name;?>-Edit-form input[type="radio"]').each(function (indx, elem) {
			$(elem).iCheck('uncheck');
		});
		$('#<?=$dataTable_name;?>-Edit-form input:not([type="checkbox"],[type="radio"]), #<?=$dataTable_name;?>-Edit-form textarea').val('');
		$('#<?=$dataTable_name;?>-Edit-form .form-group img').remove();
		$('#<?=$dataTable_name;?>-Edit-form select').val('');
		regenerate_objExtra();
	});
	<?php } ?>
	
	$('.js-<?=$dataTable_name;?>-Edit-form-submit').on('click', function (event)
	{
		event.preventDefault();
		if (validator.checkAll($('#<?=$dataTable_name;?>-Edit-form')[0])) {
			<?=$dataTable_name;?>_save()
		}
		return false;
	});
	
	$('.js-objExtra-Add').on('click', function (event) {
		var html_text = '<div class="item form-group js-objExtra">'+
		'<select class="select2_single label-control col-md-3 col-sm-3 col-xs-12 js-objExtra-inputLabel" name="" tabindex="-1">'+
			'<option value=""><?=__('No select');?></option>';
		for (var index in extra_list)
		{
			var extra_name = extra_list[index];
			html_text += '<option name="" id="" value="'+extra_name+'" class="">'+extra_name+'</option>';
		}
		html_text += '</select>'+
			'<div class="col-md-6 col-sm-6 col-xs-12">'+
				'<input type="text" name="" id="" value="" class="form-control col-md-7 col-xs-12">'+
			'</div>'+
			'<div class="col-md-3 col-sm-3 col-xs-12">'+
				'<button class="btn btn-danger btn-xs js-objExtra-Delete"><i class="fa fa-trash-o"></i> <?=__('Delete');?> </button>'+
			'</div>'+
		'</div>';
		$('#<?=$dataTable_name;?>-Edit-form').append(html_text);
	});
	
	$('#<?=$dataTable_name;?>-Edit-form').on('change', '.js-objExtra-inputLabel', function(event) {
		$(this).closest('.item.form-group').find('input.form-control').attr('name', 'extra['+$(this).val()+']');
	});
	
	$('#<?=$dataTable_name;?>-Edit-form').on('click', '.js-objExtra-Delete', function(event) {
		$(this).closest('.item.form-group').remove();
	});
	
	$('.js-<?=$dataTable_name;?>-Edit-ajax').on('click', function (event)
	{
		$('#<?=$dataTable_name;?>-Edit-ajax .modal-body').html('');
		var select_vals = $(this).closest('form').find('[name="id"]').val();
		if (select_vals.length > 0)
		{
			loader_show_in('#<?=$dataTable_name;?>-Edit-ajax .modal-content');
			$.ajax({
				url: $(this).data('route'),
				async: true,
				cache: false,
				data: {'dataTable_name' : 'table_list_ajax','webcams' : $(this).closest('.item').find('select').val(), 'player_id' : select_vals},
				type:'POST',
				dataType: "html",
				xhrFields: { withCredentials: true },
				success: function(result,status,xhr){
					$('#<?=$dataTable_name;?>-Edit-ajax .modal-body').html(result);
					dynamics.setTimeout(loader_animateSuccess, 0);
					dynamics.setTimeout(function(){
						loader_hide_in();
					}, 2000);
				},
				error: function(xhr,status,error){
					console.log( "save error" );
					dynamics.setTimeout(loader_animateError, 0);
				},
			});
		}
	});
});

function close_modal (selector)
{
	$(selector).modal("hide");
}

function regenerate_objExtra(id) {
	$('#<?=$dataTable_name;?>-Edit .js-objExtra').remove();
	if (typeof id != typeof undefined)
	{
		obj_extras = objs_extras[id];
		for (var key in obj_extras) {
			var extra = obj_extras[key];
			var html_text = '<div class="item form-group js-objExtra">'+
			'<select class="select2_single label-control col-md-3 col-sm-3 col-xs-12 js-objExtra-inputLabel" name="" tabindex="-1">'+
				'<option value=""><?=__('No select');?></option>';
				for (var index in extra_list)
				{
					var extra_name = extra_list[index];
					html_text += '<option name="" id="" value="'+extra_name+'" '+(extra_name == key ? 'selected' : '')+' class="">'+extra_name+'</option>';
				}
			html_text += '</select>'+
				'<div class="col-md-6 col-sm-6 col-xs-12">'+
					'<input type="text" name="extra['+key+']" id="" value="'+extra+'" class="form-control col-md-7 col-xs-12">'+
				'</div>'+
				'<div class="col-md-3 col-sm-3 col-xs-12">'+
					'<button class="btn btn-danger btn-xs js-objExtra-Delete"><i class="fa fa-trash-o"></i> <?=__('Delete');?> </button>'+
				'</div>'+
			'</div>';
			
			$('#<?=$dataTable_name;?>-Edit-form').append(html_text);
		}
	}
}

function <?=$dataTable_name;?>_save()
{
	
	loader_show_in($('#<?=$dataTable_name;?>-Edit-form').closest('.modal-dialog'));
	$.ajax({
		url: "<?php echo Route::url('default', array('controller' => $controller, 'action' => $actions['save'])); ?>",
		async: true,
		cache: false,
		data: new FormData($('#<?=$dataTable_name;?>-Edit-form')[0]),
		contentType: false,
		processData:false,
		type:'POST',
		dataType: "json",
		xhrFields: { withCredentials: true },
		success: function(result,status,xhr){
			if (result.status == 'OK')
			{
				if (result.datatable_index != '')
				{
					var row = <?=$dataTable_name;?>.row(result.datatable_index);
					row.data(result.row_data);
					var cells = row.nodes();
					$(cells).data('id', result.id);
					//$( cells ).removeClass('page-message--warning');
					objs_extras[result.id] = [];
					$('#<?=$dataTable_name;?>-Edit .js-objExtra').each(function(index, elem)
					{
						var inputs = $(elem).find('input');
						var key = $(inputs[0]).val();
						var extra = $(inputs[1]).val();
						objs_extras[result.id][key] = extra;
					});
				}else
				{
					var row = <?=$dataTable_name;?>.row.add(result.row_data).draw( false ).node();
					$(row).data('id', result.id);
				}
				dynamics.setTimeout(loader_animateSuccess, 0);
				dynamics.setTimeout(function(){
					$('#<?=$dataTable_name;?>-Edit').modal('hide');
					$('#<?=$dataTable_name;?>-Edit').off('hidden.bs.modal').on('hidden.bs.modal', function () {
						loader_hide_in();
					});
				}, 1500);
			} else
			{
				console.log(result.message);
				dynamics.setTimeout(loader_animateError, 0);
			}
		},
		error: function(xhr,status,error){
			console.log( "save error" );
			dynamics.setTimeout(loader_animateError, 0);
		},
	});
}

</script>
