<div class="">
	<div class="page-title">
		<div class="title_left">
			<h3><?php echo $title; ?></h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="<?=$dataTable_name;?>" class="table table-striped table-bordered dt-responsive responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<?php foreach ($list_columns as $column_name=>$column_info) {
								if ($column_info['list_view'])
								{
									echo "<th class=\"".$column_name."\" data-name=\"".$column_name."\" data-db_type=\"".$column_info['data_type']."\" data-key=\"".$column_info['key']."\">$column_name</th>";
								}
							} ?>
							<?php foreach ($has_many as $column_name=>$column_info) {
								isset($column_info['list_view']) OR $column_info['list_view'] = TRUE;
								if ($column_info['list_view'])
								{
									echo "<th data-name=\"".$column_name."\">$column_name</th>";
								}
							} ?>
							<?php if ($show_actions['edit'] == TRUE OR $show_actions['delete'] == TRUE) { ?>
								<th><?=__('datatables_actions');?></th>
							<?php } ?>
						</tr>
					</thead>
						<tbody>
						<?php $objs_extras = array();
						$sort_check = FALSE;
						$sort_position = 0;
						$sorter = 1;
						foreach ($orm_objs as $orm_obj) {
							echo '<tr data-id="'.$orm_obj->pk().'">';
							$iterator = 0;
							foreach ($orm_obj->as_array() as $key=>$value) {
								if ($key == 'extra')
								{
									$objs_extras[$orm_obj->pk()] = $orm_obj->get($key);
								} else if ($key == 'sort')
								{
									$sort_check = TRUE;
									$sort_position = $iterator;
								}
								$column_info = Arr::get($list_columns, $key);
								if ($column_info['list_view'])
								{
									if ($column_info['data_type'] == 'timestamp' AND $value != NULL) {
										echo "<td class=\"$key\">".DateTime::createFromFormat('U',$value)->format('Y-m-d H:i:s')."</td>";
									} else if (strpos($column_info['data_type'], 'blob') !== FALSE) {
										if (isset($value) AND $value != NULL) {
											echo "<td class=\"$key table-img-wrapper\">"."<img src=\"data:image/gif;base64," . base64_encode($value) . "\" style=\"max-width:100%;\" />"."</td>";
										} else {
											echo "<td class=\"$key table-img-wrapper\">"."no file"."</td>";
										}
									} else if (is_array($value)) {
										echo "<td class=\"$key\">";
										foreach ($value as $extra_key=>$extra_value)
										{
											echo $extra_key.':'.$extra_value.'<hr>';
										}
										echo "</td>";
									} else if ($key == 'sort') {
										echo "<td class=\"$key\">$sorter</td>";
									} else {
										echo "<td class=\"$key\">$value</td>";
									}
									$iterator++;
								}
							}
							foreach ($orm_obj->has_many() as $column_name=>$column_info) {
								isset($column_info['list_view']) OR $column_info['list_view'] = TRUE;
								if ($column_info['list_view'])
								{
									echo "<td class=\"$column_name\">";
									$first = TRUE;
									foreach ($orm_obj->get($column_name)->find_all() as $many_key=>$many_value)
									{
										if (!$first)
										{
											echo "<br>";
										}
										echo Form::hidden($column_name.'[]', $many_value->pk());
										echo (array_key_exists('name',$many_value->as_array()) ? $many_value->get('name') : $many_value->pk());
										$first = FALSE;
									}
									echo "</td>";
								}
							}
							if ($show_actions['edit'] == TRUE OR $show_actions['delete'] == TRUE) {
								echo '<td></td>';
							}
							echo "</tr>";
							$sorter++;
						} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
var <?=$dataTable_name;?>;
var objs_extras = <?=json_encode($objs_extras);?>;
var extra_list = <?=json_encode($extra_list);?>;

$(document).ready(function() {
	<?=$dataTable_name;?> = $('#<?=$dataTable_name;?>').DataTable({
		"dom": '<"toolbar">lfrtip',
		<?php if ($sort_check == TRUE) { ?>
		"order": [<?=$sort_position;?>, 'asc'],
		"rowReorder": {
			"selector": '#<?=$dataTable_name;?> tbody .sort',
			dataSrc: <?=$sort_position;?>,
		},
		<?php } ?>
		<?php if ($show_actions['edit'] == TRUE OR $show_actions['delete'] == TRUE) {
			$defaultContent = "";
			if ($show_actions['edit'] == TRUE) {
				$defaultContent .= "<a href=\"#$dataTable_name-Edit\" data-toggle=\"modal\" class=\"btn btn-info btn-xs js-$dataTable_name-Edit\"><i class=\"fa fa-pencil\"></i> Edit </a>";
			}
			if ($show_actions['delete'] == TRUE) {
				$defaultContent .= "<a href=\"#\" class=\"btn btn-danger btn-xs js-dataTables-Delete\"><i class=\"fa fa-trash-o\"></i> Delete </a>";
			}
			if ($orm_name == 'Player') {
				$defaultContent .= "<a href=\"#\" class=\"btn btn-default btn-xs js-dataTables-codeGenerator\"><i class=\"fa fa-code\"></i> Generate Code </a>";
			} ?>
		"fnCreatedRow": function( nRow, aData, iDataIndex ) {
			$(nRow).data('obj_id', aData['data_obj_id']);
			$(nRow).find('a.js-<?=$dataTable_name;?>-Edit').data('obj_id', aData['data_obj_id']);
		},
		<?php } ?>
		"columnDefs": [
		<?php if ($show_actions['edit'] == TRUE OR $show_actions['delete'] == TRUE) { ?>
			{
				"targets": -1,
				"searchable": false,
				"orderable": false,
				"visible": true,
				"className": "all",
				"data": null,
				"defaultContent": '<?=$defaultContent?>',
			},
		<?php } ?>
		<?php if ($sort_check == TRUE) { ?>
			{
				"targets": 'sort',
				"orderable": true,
				"className": 'reorder'
			},
			{
				"orderable": false,
				"targets": '_all'
			}
		<?php } ?>
		],
	});
	
	<?php if ($sort_check == TRUE) { ?>
	<?=$dataTable_name;?>.on( 'row-reorder', function (event, diff, edit) {
		var ids = [];
		var positions = [];
		for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
			ids.push(parseInt(<?=$dataTable_name;?>.row( diff[i].node ).data()[0]));
			positions.push(diff[i].newData);
		}
		<?=$dataTable_name;?>_save_sort(ids, positions);
	} );
	<?php } ?>
	
	$('#<?=$dataTable_name;?>').on('click', 'tbody tr td a.js-dataTables-Delete', function (event) {
		event.preventDefault();
		var id = $(this).closest('tr').data('id');
		var datatable_index = <?=$dataTable_name;?>.row( $(this).closest('tr') ).index();
		<?=$dataTable_name;?>_del(id, datatable_index);
		return false;
	});
	
	$('#<?=$dataTable_name;?>').on('click', 'tbody tr td a.js-dataTables-codeGenerator', function (event) {
		event.preventDefault();
		var id = $(this).closest('tr').data('id');
		<?=$dataTable_name;?>_codeGenerator(id);
		return false;
	});
	
});

function <?=$dataTable_name;?>_del(id, datatable_index)
{
	loader_show_in('#<?=$dataTable_name;?>_wrapper');
	$.ajax({
		url: "<?php echo Route::url('default', array('controller' => $controller, 'action' => $actions['delete'])); ?>",
		async: true,
		cache: false,
		data: {'id': id, 'orm_name': '<?=$orm_name?>'},
		type:'POST',
		dataType: "json",
		xhrFields: { withCredentials: true },
		success: function(result,status,xhr){
			if (result.status == 'OK')
			{
				<?=$dataTable_name;?>.row(datatable_index).remove().draw( false );
				dynamics.setTimeout(loader_animateSuccess, 0);
				dynamics.setTimeout(function(){
					loader_hide_in();
				}, 1500);
			} else
			{
				console.log(result.message);
				dynamics.setTimeout(loader_animateError, 0);
				dynamics.setTimeout(function(){
					loader_hide_in();
				}, 3000);
			}
		},
		error: function(xhr,status,error){
			console.log( "save error" );
			dynamics.setTimeout(loader_animateError, 0);
			dynamics.setTimeout(function(){
				loader_hide_in();
			}, 3000);
		},
	});
}

<?php if ($sort_check == TRUE) { ?>
function <?=$dataTable_name;?>_save_sort(ids, positions)
{
	if( (typeof ids != typeof undefined && ids.length > 0) || (typeof positions != typeof undefined && positions.length > 0) )
	{
		loader_show_in('#<?=$dataTable_name;?>_wrapper');
		$.ajax({
			url: "<?php echo Route::url('default', array('controller' => $controller, 'action' => $actions['sort'])); ?>",
			async: true,
			cache: false,
			data: {'ids': ids, 'positions': positions, 'orm_name': '<?=$orm_name?>'},
			type:'POST',
			dataType: "json",
			xhrFields: { withCredentials: true },
			success: function(result,status,xhr){
				if (result.status == 'OK')
				{
					dynamics.setTimeout(loader_animateSuccess, 0);
					dynamics.setTimeout(function(){
						loader_hide_in();
					}, 1500);
				} else
				{
					console.log(result.message);
					dynamics.setTimeout(loader_animateError, 0);
					dynamics.setTimeout(function(){
						loader_hide_in();
					}, 3000);
				}
			},
			error: function(xhr,status,error){
				console.log( "save error" );
				dynamics.setTimeout(loader_animateError, 0);
				dynamics.setTimeout(function(){
					loader_hide_in();
				}, 3000);
			},
		});
	}
}
<?php } ?>

<?php if ($orm_name == 'Player') { ?>
function <?=$dataTable_name;?>_codeGenerator(id)
{
	loader_show_in();
	$('#js-modal-code_generate-player_'+id).remove();
	$.ajax({
		url: "<?php echo Route::url('default', array('controller' => $controller, 'action' => $actions['codeGenerator'])); ?>",
		async: true,
		cache: false,
		data: {'player_id' : id},
		type:'POST',
		dataType: "json",
		xhrFields: { withCredentials: true },
		success: function(result,status,xhr){
			var new_modal = $(result.edit_view);
			$('#js-template-container-body').after(new_modal);
			dynamics.setTimeout(loader_animateSuccess, 0);
			dynamics.setTimeout(function(){
				loader_hide_in();
				new_modal.modal('show');
			}, 1500);
		},
		error: function(xhr,status,error){
			console.log( "save error" );
			dynamics.setTimeout(loader_animateError, 0);
			dynamics.setTimeout(function(){
				loader_hide_in();
			}, 3000);
		},
	});
}
<?php } ?>

</script>
