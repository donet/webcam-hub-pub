<?php if ($show_actions['edit'] == TRUE) { ?>
<div id="<?=$dataTable_name;?>-Edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="dataTable-Edit-title"></h4>
			</div>
			<div class="modal-body">
				<form id="<?=$dataTable_name;?>-Edit-form" class="form-horizontal form-label-left">
					<?=Form::hidden('datatable_index', '');?>
					<?php foreach ($list_columns as $column_name=>$column_info) {
					if($column_info['edit_view'] AND $column_name != 'extra') {
						if ($column_info['type'] == 'int' AND strpos($column_info['data_type'], 'int') !== FALSE AND $column_info['extra'] == 'auto_increment') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="number" name="<?=$column_name;?>" id="<?=$column_name;?>" min="0" step="1" max="" pattern="\d" readonly class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'int' AND $column_info['data_type'] == 'tinyint' AND $column_info['display'] == '1') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<label>
									<input type="checkbox" name="<?=$column_name;?>" id="<?=$column_name;?>" class="js-switch" />
								</label>
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'int' AND strpos($column_info['data_type'], 'int') !== FALSE AND $column_info['key'] == 'MUL') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="select2_single form-control" name="<?=$column_name;?>" tabindex="-1" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?>>
                            <option value=""><?=__('No select');?></option>
                            <?php if ($column_name == 'company_id' AND !Auth::instance()->logged_in('superadmin'))
                            {
										 $options = $user->get('companies')->find_all();
									 } else
									 {
										 $options = ORM::factory($column_info['model'])->find_all();
									 }
                            foreach ($options as $option) { ?>
										<option value="<?=$option->pk();?>"><?=(array_key_exists('name', $option->as_array()) == TRUE ? $option->get('name') : $option->pk());?></option>
                            <?php } ?>
                          </select>
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'int' AND strpos($column_info['data_type'], 'int') !== FALSE) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="number" name="<?=$column_name;?>" id="<?=$column_name;?>" min="0" step="1" max="" pattern="\d" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					<?php } else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'date')) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="js-datepicker form-control col-md-7 col-xs-12" name="<?=$column_name;?>" id="<?=$column_name;?>" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> placeholder="">
							</div>
						</div>
						<script>
							$(document).ready(function() {
								$('.js-datepicker').daterangepicker({
									"singleDatePicker": true,
									"timePicker": false,
									"locale": {
										"format": "YYYY-MM-DD",
									},
								});
							});
						</script>
					<?php } else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'time')) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="js-timepicker form-control col-md-7 col-xs-12" name="<?=$column_name;?>" id="<?=$column_name;?>" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> placeholder="">
							</div>
						</div>
						<script>
							$(document).ready(function() {
								$('.js-timepicker').daterangepicker({
									"singleDatePicker": true,
									"timePicker": true,
									"timePickerSeconds": true,
									"timePicker24Hour": true,
									"locale": {
										"format": "HH:mm:ss",
									},
								});
								$('.js-timepicker').on('show.daterangepicker', function(ev, picker) {
									picker.container.find('.calendar-table').addClass('js-hide');
								});
								$('.js-timepicker').on('hide.daterangepicker', function(ev, picker) {
									picker.container.find('.calendar-table').removeClass('js-hide');
								});
							});
						</script>
					<?php } else if ($column_info['type'] == 'string' AND ($column_info['data_type'] == 'datetime' OR $column_info['data_type'] == 'timestamp')) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="hidden" class="js-datetimepicker-timestamp" name="<?=$column_name;?>-timestamp" id="<?=$column_name;?>" novaluefromdt >
								<input type="text" class="js-datetimepicker form-control col-md-7 col-xs-12" name="<?=$column_name;?>" id="<?=$column_name;?>" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> placeholder="">
							</div>
						</div>
						<script>
							$(document).ready(function() {
								$('.js-datetimepicker').daterangepicker({
									"singleDatePicker": true,
									"timePicker": true,
									"timePickerSeconds": true,
									"timePicker24Hour": true,
									"locale": {
										"format": "YYYY-MM-DD HH:mm:ss",
									},
								});
								$('.js-datetimepicker').on('hide.daterangepicker', function(ev, picker) {
									picker.element.siblings().val(moment(picker.element.val()+'Z').format('X'))
								});
							});
						</script>
					<?php } else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'enum')) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="radio">
									<?php foreach ($column_info['options'] as $option) { ?>
									<label>
										<input type="radio" class="flat" name="<?=$column_name;?>" value="<?=$option;?>"> <?=$option;?>
									</label>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } else if (($column_info['type'] == 'string' AND $column_info['data_type'] == 'set')) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="checkbox">
									<?php foreach ($column_info['options'] as $option) { ?>
									<label>
										<input type="checkbox" class="flat" name="<?=$column_name;?>" value="<?=$option;?>"> <?=$option;?>
									</label>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'string' AND strpos($column_info['data_type'], 'blob') !== FALSE) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="file" name="<?=$column_name;?>" id="<?=$column_name;?>" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					<?php }else if ($column_info['type'] == 'string' AND $column_info['data_type'] == 'varchar') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" name="<?=$column_name;?>" id="<?=$column_name;?>" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					<?php } else if ($column_info['type'] == 'string' AND $column_info['data_type'] == 'text') { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<textarea class="form-control" name="<?=$column_name;?>" id="<?=$column_name;?>" rows="3" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> placeholder=''></textarea>
							</div>
						</div>
					<?php }
						}
					} ?>
					<?php foreach ($has_many as $column_name=>$column_info) {
						isset($column_info['required']) OR $column_info['required'] = FALSE;
						isset($column_info['edit_view']) OR $column_info['edit_view'] = TRUE;
						if ($column_name == 'companies' AND !Auth::instance()->logged_in('superadmin')) {
							$column_info['edit_view'] = FALSE;
						}
						if($column_info['edit_view']) { ?>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="<?=$column_name;?>"><?=$column_name;?>
							<?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? '<span class="required">*</span>' : ''?>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="select2_single form-control" name="<?=$column_name;?>[]" data-foreing="name" tabindex="-1" <?=($column_info['required'] === NULL OR $column_info['required'] !== NULL AND $column_info['required'])? 'required="required"' : ''?> multiple>
									<option value=""><?=__('No select');?></option>
									<?php if ($column_info['model'] == 'Role' AND !Auth::instance()->logged_in('superadmin'))
									{
										$options = ORM::factory($column_info['model'])->where('name', '!=', 'superadmin')->find_all();
									} else
									{
										$options = ORM::factory($column_info['model'])->find_all();
									}
									foreach ($options as $option) { ?>
											<option value="<?=$option->pk();?>"><?=(array_key_exists('name', $option->as_array()) == TRUE ? $option->get('name') : $option->pk());?></option>
									<?php } ?>
								</select>
							</div>
							<?php if($column_info['edit_view'] AND $column_name == 'webcams') { ?>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<a href="#<?=$dataTable_name;?>-Edit-ajax" data-toggle="modal" class="btn btn-info btn-xs js-<?=$dataTable_name;?>-Edit-ajax" data-route="<?php echo Route::url('default', array('controller' => 'WebcamHubManager', 'action' => 'webcamsPlayerList')); ?>"><i class="fa fa-pencil"></i> Edit </a>
							</div>
							<script>
								$('[name="<?=$column_name;?>[]"]').on('change', function(event)
								{
									$('.js-<?=$dataTable_name;?>-Edit-ajax').hide();
								});
							</script>
							<?php } ?>
						</div>
					<?php }
					} ?>
				</form>
			</div>
			<div class="modal-footer">
				<?php $column_info = Arr::get($list_columns, 'extra');
				if (false AND $column_info != NULL AND $column_info['edit_view']) { // show only in old behaviour, now all extra are visible ?>
				<button class="btn btn-success btn-xs js-objExtra-Add"><i class="fa fa-plus"></i> <?=__('Add Extra');?> </button>
				<?php } ?>
				<button class="btn btn-default" onclick="close_modal('#<?=$dataTable_name;?>-Edit');">Close</button>
				<button type="button" form="<?=$dataTable_name;?>-Edit-form" class="btn btn-primary js-<?=$dataTable_name;?>-Edit-form-submit">Save</button>
			</div>
		</div>
	</div>
</div>

<div id="<?=$dataTable_name;?>-Edit-ajax" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="dataTable-Edit-title"></h4>
			</div>
			<div class="modal-body" style="min-height:120px;">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<script>

$(document).ready(function() {
	<?php if ($show_actions['edit'] == TRUE) { ?>
	// Apply the edit
	$('#<?=$dataTable_name;?>').on('click', 'tbody tr td a.js-<?=$dataTable_name;?>-Edit', function (event) {
		loader_hide_in();
		$('#<?=$dataTable_name;?>-Edit-form input:not([type="checkbox"],[type="radio"]), #<?=$dataTable_name;?>-Edit-form textarea').val('');
		$('.js-<?=$dataTable_name;?>-Edit-ajax').show();
		row_datas = $(this).closest('td').siblings();
		$('#<?=$dataTable_name;?>-Edit-form input[name="datatable_index"]:not([novaluefromdt])').val(<?=$dataTable_name;?>.row( $(this).closest('tr') ).index());
		$(this).closest('table').find('thead th').each(function(indx, elem) {
			var data_type = $(elem).data('db_type');
			var data_key = $(elem).data('key');
			var data_extra = $(elem).data('extra');
			var data_name = $(elem).data('name');
			if (typeof data_type != typeof undefined) {
				if (data_type.indexOf('int') !== -1) {
					if (data_key.indexOf('MUL') !== -1) {
						value = $(row_datas[indx]).text();
						$('#<?=$dataTable_name;?>-Edit-form select[name="'+data_name+'"]:not([novaluefromdt]) option').each(function (indx, elem) {
							$(elem).prop('selected', $(elem).val() == value);
						});
					} else if (data_name == 'id') {
						$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).text());
						regenerate_objExtra_all(parseInt($(row_datas[indx]).text()))
					} else {
						$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).text());
					}
				} else if (data_type == 'varchar') {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).text());
				} else if (data_type == 'text') {
					$('#<?=$dataTable_name;?>-Edit-form textarea[name="'+data_name+'"]:not([novaluefromdt])').val($(row_datas[indx]).text());
				} else if (data_type == 'date') {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setStartDate($(row_datas[indx]).text());
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setEndDate($(row_datas[indx]).text());
				} else if (data_type == 'time') {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setStartDate($(row_datas[indx]).text());
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker').setEndDate($(row_datas[indx]).text());
				} else if (data_type == 'datetime') {
					picker = $('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker');
					picker.setStartDate($(row_datas[indx]).text());
					picker.setEndDate($(row_datas[indx]).text());
					picker.element.siblings().val(moment(picker.element.val()+'Z').format('X'))
				} else if (data_type == 'timestamp') {
					picker = $('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').data('daterangepicker');
					picker.setStartDate($(row_datas[indx]).text());
					picker.setEndDate($(row_datas[indx]).text());
					picker.element.siblings().val(moment(picker.element.val()+'Z').format('X'))
				} else if (data_type == 'tinyint') {
					changeSwitcheryState($('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])'), parseInt($(row_datas[indx]).text()) == 1);
				} else if (data_type == 'enum') {
					value = $(row_datas[indx]).text();
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').each(function(indx, elem) {
						if ($(elem).val() == value) {
							$(elem).iCheck('check');
						} else {
							$(elem).iCheck('uncheck');
						}
					})
				} else if (data_type == 'set') {
					value = $(row_datas[indx]).text();
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').each(function(indx, elem) {
						if (value.indexOf($(elem).val()) !== -1) {
							$(elem).iCheck('check');
						} else {
							$(elem).iCheck('uncheck');
						}
					})
				} else if (data_type.indexOf('blob') !== -1) {
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').siblings().remove();
					$('#<?=$dataTable_name;?>-Edit-form input[name="'+data_name+'"]:not([novaluefromdt])').before($(row_datas[indx]).html());
				}
			} else
			{
				// attencion Action column has data_name undefined
				if (typeof data_name != typeof undefined) {
					// has_many elements
					var dataarray = [];
					$(row_datas[indx]).find('[name="'+data_name+'[]"]').each(function(){
						dataarray.push($(this).val());
					});
					if (dataarray.length > 0)
					{
						$('#<?=$dataTable_name;?>-Edit-form select[name="'+data_name+'[]"]:not([novaluefromdt])').val(dataarray);
					} else
					{
						$('#<?=$dataTable_name;?>-Edit-form select[name="'+data_name+'[]"]:not([novaluefromdt])').val('');
					}
				}
			}
		})
	});
	<?php } ?>

	<?php if ($show_actions['add'] == TRUE) { ?>
	// Add elem
	$("div.toolbar").html("<a href=\"#<?=$dataTable_name;?>-Edit\" data-toggle=\"modal\" class=\"btn btn-success btn-xs js-dataTables-Add\"><i class=\"fa fa-plus\"></i> Add </a>").css('display', 'inline-block');
	$('.js-dataTables-Add').on('click', function (event) {
		loader_hide_in();
		$('.js-<?=$dataTable_name;?>-Edit-ajax').hide();
		changeSwitcheryState($('#<?=$dataTable_name;?>-Edit-form input[type="checkbox"]'), false);
		$('#<?=$dataTable_name;?>-Edit-form input[type="checkbox"]').each(function (indx, elem) {
			$(elem).iCheck('uncheck');
		});
		$('#<?=$dataTable_name;?>-Edit-form input[type="radio"]').each(function (indx, elem) {
			$(elem).iCheck('uncheck');
		});
		$('#<?=$dataTable_name;?>-Edit-form input:not([type="checkbox"],[type="radio"]), #<?=$dataTable_name;?>-Edit-form textarea').val('');
		$('#<?=$dataTable_name;?>-Edit-form .form-group img').remove();
		$('#<?=$dataTable_name;?>-Edit-form select').val('');
		regenerate_objExtra_all();
	});
	<?php } ?>
	
	$('.js-<?=$dataTable_name;?>-Edit-form-submit').on('click', function (event)
	{
		event.preventDefault();
		if (validator.checkAll($('#<?=$dataTable_name;?>-Edit-form')[0])) {
			<?=$dataTable_name;?>_save()
		}
		return false;
	});
	
	$('.js-objExtra-Add').on('click', function (event) {
		var html_text = regenerate_objExtra_item_group('', '');
		$('#<?=$dataTable_name;?>-Edit-form').append(html_text);
	});
	
	$('#<?=$dataTable_name;?>-Edit-form').on('change', '.js-objExtra-inputLabel', function(event) {
		var extra_key = $(this).val();
		var extra_value = $(this).closest('.item.form-group').find('.js-objExtra-formControl').val();
		var html_text_input = regenerate_objExtra_field(extra_key, extra_value);
		var obj_html = $(html_text_input);
		$(this).next("div").remove();
		$(this).after(obj_html);
		obj_html.find('.js-colorpicker').colorpicker();
		obj_html.find('.js-switch').each(function (index, elem) {
				var switchery = new Switchery(elem, {
					color: '#26B99A'
				});
			});
		$(this).closest('.item.form-group').find('.js-objExtra-formControl').attr('name', 'extra['+extra_key+']');
	});
	
	$('#<?=$dataTable_name;?>-Edit-form').on('click', '.js-objExtra-Delete', function(event) {
		$(this).closest('.item.form-group').remove();
	});
	
	$('.js-<?=$dataTable_name;?>-Edit-ajax').on('click', function (event)
	{
		$('#<?=$dataTable_name;?>-Edit-ajax .modal-body').html('');
		var select_vals = $(this).closest('form').find('[name="id"]').val();
		if (select_vals.length > 0)
		{
			loader_show_in('#<?=$dataTable_name;?>-Edit-ajax .modal-content');
			$.ajax({
				url: $(this).data('route'),
				async: true,
				cache: false,
				data: {'dataTable_name' : 'table_list_ajax','webcams' : $(this).closest('.item').find('select').val(), 'player_id' : select_vals},
				type:'POST',
				dataType: "json",
				xhrFields: { withCredentials: true },
				success: function(result,status,xhr){
					$('#<?=$dataTable_name;?>-Edit-ajax .modal-body').html(result.list_view);
					$('#js-template-container-body').after(result.edit_view);
					dynamics.setTimeout(loader_animateSuccess, 0);
					dynamics.setTimeout(function(){
						loader_hide_in();
					}, 1500);
				},
				error: function(xhr,status,error){
					console.log( "ajax error" );
					dynamics.setTimeout(loader_animateError, 0);
					dynamics.setTimeout(function(){
						loader_hide_in();
					}, 3000);
				},
			});
		}
	});
});

function close_modal (selector)
{
	$(selector).modal("hide")
}

function regenerate_objExtra(id)
{
	$('#<?=$dataTable_name;?>-Edit .js-objExtra').remove();
	if (typeof id != typeof undefined)
	{
		obj_extras = objs_extras[id];
		for (var extra_key in obj_extras) {
			var extra_value = obj_extras[extra_key];
			var html_text = regenerate_objExtra_item_group(extra_key, extra_value);
			var obj_html = $(html_text);
			$('#<?=$dataTable_name;?>-Edit-form').append(obj_html);
			obj_html.find('.js-colorpicker').colorpicker();
			obj_html.find('.js-switch').each(function (index, elem) {
				var switchery = new Switchery(elem, {
					color: '#26B99A'
				});
			});
		}
	}
}

function regenerate_objExtra_all(id)
{
	$('#<?=$dataTable_name;?>-Edit .js-objExtra').remove();
	var html_text = '';
	for (var extra_name in extra_list)
	{
		var extra_info = extra_list[extra_name];
		if (typeof extra_info.edit_view == typeof undefined || (typeof extra_info.edit_view != typeof undefined && extra_info.edit_view))
		{
			html_text += '<div class="item form-group js-objExtra">';
			html_text += '<label class="control-label col-md-3 col-sm-3 col-xs-12">'+extra_name+'</label>';
			if (typeof id != typeof undefined && objs_extras[id] != null)
			{
				var obj_extras = objs_extras[id];
				var extra_value = obj_extras[extra_name];
				html_text += regenerate_objExtra_field(extra_name, extra_value);
			} else
			{
				html_text += regenerate_objExtra_field(extra_name, undefined);
			}
			html_text += '</div>';
		}
	}
	var obj_html = $(html_text);
	$('#<?=$dataTable_name;?>-Edit-form').append(obj_html);
	obj_html.find('.js-colorpicker').colorpicker();
	obj_html.find('.js-switch').each(function (index, elem) {
		var switchery = new Switchery(elem, {
			color: '#26B99A'
		});
	});
}

function regenerate_objExtra_item_group(extra_key, extra_value)
{
	if (typeof extra_value != typeof undefined)
	{
		var html_text = '<div class="item form-group js-objExtra">'+
		'<select class="select2_single label-control col-md-3 col-sm-3 col-xs-12 js-objExtra-inputLabel" name="" tabindex="-1">'+
			'<option value=""><?=__('No select');?></option>';
		for (var extra_name in extra_list)
		{
			var extra_info = extra_list[extra_name];
			html_text += '<option name="" id="" value="'+extra_name+'" '+(extra_name == extra_key ? 'selected' : '')+' class="">'+extra_name+'</option>';
		}
		html_text += '</select>';
		html_text_input = regenerate_objExtra_field(extra_key, extra_value);
		html_text += html_text_input+'<div class="col-md-3 col-sm-3 col-xs-12">'+
				'<button class="btn btn-danger btn-xs js-objExtra-Delete"><i class="fa fa-trash-o"></i> <?=__('Delete');?> </button>'+
			'</div>'+
		'</div>';
	} else
	{
		html_text = '';
	}
	return html_text;
}

function regenerate_objExtra_field(extra_key, extra_value)
{
	html_text_input = '<div class="col-md-6 col-sm-6 col-xs-12">';
	if (typeof extra_key == typeof undefined || extra_key == '' ) {
		html_text_input += '<input type="text" name="" id="" value="" class="js-objExtra-formControl form-control col-md-7 col-xs-12">';
	} else
	{
		var extra_info = extra_list[extra_key];
		if (typeof extra_value == typeof undefined)
		{
			if (typeof extra_info.default_value != typeof undefined)
			{
				extra_value = extra_info.default_value;
			} else
			{
				extra_value = '';
			}
		}
		if (typeof extra_info != typeof undefined && extra_info.type == 'string') {
			if (extra_info.data_type == 'colorpicker') {
				html_text_input += '<div class="input-group js-colorpicker">'+
					'<input type="text" name="extra['+extra_key+']" id="" value="'+extra_value+'" class="js-objExtra-formControl form-control col-md-7 col-xs-12">'+
					'<span class="input-group-addon"><i></i></span></div>';
			} else if (extra_info.data_type == 'varchar') {
				html_text_input += '<input type="text" name="extra['+extra_key+']" id="" value="'+extra_value+'" class="js-objExtra-formControl form-control col-md-7 col-xs-12">';
			} else {
				html_text_input += '<input type="text" name="extra['+extra_key+']" id="" value="'+extra_value+'" class="js-objExtra-formControl form-control col-md-7 col-xs-12">';
			}
		} else if (extra_info.type.indexOf('int') !== -1 && extra_info.data_type.indexOf('tinyint') !== -1 && extra_info.display.indexOf('1') !== -1) {
			html_text_input += '<div class="input-group">'+
				'<label>'+
					'<input type="checkbox" name="extra['+extra_key+']" id="" value="yes" class="js-switch"'+(extra_value == 'yes' ? 'checked' : '')+' />'+
				'</label>'+
			'</div>';
		} else {
			html_text_input += '<input type="text" name="extra['+extra_key+']" id="" value="'+extra_value+'" class="js-objExtra-formControl form-control col-md-7 col-xs-12">';
		}
	}
	html_text_input += '</div>';
	return html_text_input;
}

function <?=$dataTable_name;?>_save()
{
	loader_show_in($('#<?=$dataTable_name;?>-Edit-form').closest('.modal-dialog'));
	var send_form_data = new FormData($('#<?=$dataTable_name;?>-Edit-form')[0]);
	$('#<?=$dataTable_name;?>-Edit-form .js-switch:not(:checked)').each(function(index, elem)
	{
		send_form_data.set(elem.name, 'no');
	});
	$.ajax({
		url: "<?php echo Route::url('default', array('controller' => $controller, 'action' => $actions['save'])); ?>",
		async: true,
		cache: false,
		data: send_form_data,
		contentType: false,
		processData:false,
		type:'POST',
		dataType: "json",
		xhrFields: { withCredentials: true },
		success: function(result,status,xhr){
			if (result.status == 'OK')
			{
				if (result.datatable_index != '')
				{
					var row = <?=$dataTable_name;?>.row(result.datatable_index);
					row.data(result.row_data);
					var cells = row.nodes();
					$(cells).data('id', result.id);
					//$( cells ).removeClass('page-message--warning');
					objs_extras[result.id] = [];
					$('#<?=$dataTable_name;?>-Edit .js-objExtra').each(function(index, elem)
					{
						if (false)
						{
							//old behaviour
							var key = $(elem).find('select').val();
							var extra = $(elem).find('input:not([type="checkbox"]), input[type="checkbox"]:checked').val();
							objs_extras[result.id][key] = extra;
						} else
						{
							var key = $(elem).find('label').text();
							var extra = undefined;
							var extra_elem = $(elem).find('input:not([type="checkbox"]), input[type="checkbox"]:checked');
							if (extra_elem.length > 0)
							{
								extra = extra_elem.val();
							} else
							{
								var extra_elem = $(elem).find('.js-switch:not(:checked)');
								if (extra_elem.length > 0)
								{
									extra = 'no';
								}
							}
							objs_extras[result.id][key] = extra;
						}
					});
				}else
				{
					var row = <?=$dataTable_name;?>.row.add(result.row_data).draw( false ).node();
					$(row).data('id', result.id);
				}
				dynamics.setTimeout(loader_animateSuccess, 0);
				dynamics.setTimeout(function(){
					$('#<?=$dataTable_name;?>-Edit').modal('hide');
					$('#<?=$dataTable_name;?>-Edit').off('hidden.bs.modal').on('hidden.bs.modal', function () {
						loader_hide_in();
					});
				}, 1500);
			} else
			{
				console.log(result.message);
				dynamics.setTimeout(loader_animateError, 0);
				dynamics.setTimeout(function(){
					loader_hide_in();
				}, 3000);
			}
		},
		error: function(xhr,status,error){
			console.log( "save error" );
			dynamics.setTimeout(loader_animateError, 0);
			dynamics.setTimeout(function(){
				loader_hide_in();
			}, 3000);
		},
	});
}

</script>
