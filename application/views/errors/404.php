<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title><?php echo __('project_title');?> | 404</title>
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?=Utils::getURLinVersion($_REQUEST, "media/images/wequid-icon.png")?>" type="image/png">
		<!-- APPCONS -->
		<link rel="apple-touch-icon" href="<?=Utils::getURLinVersion($_REQUEST, "media/images/wequid-icon.png")?>">

    <!-- Bootstrap -->
    <link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/bootstrap/dist/css/bootstrap.min.css");?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/nprogress/nprogress.css");?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=Utils::getURLinVersion($_REQUEST ,"media/css/custom.css");?>" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Sorry but we couldn't find this page</h2>
              <h2><?php echo $message ?></h2>
              <p>This page you are looking for does not exist <a href="#">Report this?</a>
              </p>
              <div class="mid_center">
                <a href="<?php echo Route::url('default'); ?>" class="site_title"><span><?php echo __('project_title');?></span></a>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/jquery/dist/jquery.min.js");?>"></script>
    <!-- Bootstrap -->
    <script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/bootstrap/dist/js/bootstrap.min.js");?>"></script>
    <!-- FastClick -->
    <script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/fastclick/lib/fastclick.js");?>"></script>
    <!-- NProgress -->
    <script src="<?=Utils::getURLinVersion($_REQUEST ,"media/vendors/nprogress/nprogress.js");?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?=Utils::getURLinVersion($_REQUEST ,"media/js/custom.min.js");?>"></script>
  </body>
</html>
