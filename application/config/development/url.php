<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	'trusted_hosts' => array(
		'dev\.wequid_cdh\.it',
		'dev\.webcam-hub\.u16',
		'dev\.webcam-hub\.it',
		'webcam-hub\.wequid\.com',
		'prod\.wequid\.com',
		'webcam-hub\.wequid\.com',
    ),
);
