<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(

	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => ':dO|n`S@\n^LUY2Hmch|djpY$Q2G^f/w|jijPH;%LU&$^:fD-{<+0<pDh3ZD"zK',
	'lifetime'     => Date::YEAR * 1, // Date::YEAR Date::MONTH Date::HOUR Date::MINUTE
	'session_type' => Session::$default,
	'session_key'  => 'auth_user',
);
