<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'language' => array(
			'en',
			'it',
	),
	'user'	=> array(
		'create'	=> array(
			'username'	=> array(
				'min_length'	=> 4,
				'max_length'	=> 32,
			),
			'password'	=> array(
				'min_length'	=> 6,
			)
		),
	),
);
