<!DOCTYPE html>
<html class="zs">
	<head>
		<meta http-equiv="refresh" content="9999999999999999">
		<title>Webcam Hub</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Zostrap CSS -->
		<link id="js-webcam-hub-theme" rel='stylesheet' href='./css/themes/default.css' type='text/css' media='all' />
		<!-- Zostrap JS -->
		<script type="text/javascript" src="./zostrap/js/zostrap.js"></script>
	</head>
	<body class="xs-relative">
		<!-- <div class="no-mobile-portrait xs-absolute xs-l0 xs-r0 xs-t0 xs-b0">
			<div class="xs-center">NOOOOO</div>
		</div> -->
		<div id="js-webcam-hub" class="grid webcam-hub">
			<div id ="js-left-sidebar" class="left-sidebar sidebar-open gc xs-gc12 xs-relative sm-gc4 lg-gc3 xxl-gc2 xs-z2 xs-order1 sm-order0">
				<div id="js-left-sidebar-open_close" class="left-sidebar-open_close xs-hide xs-text-right sm-block">
					<button class="left-sidebar-open_close-button xs-relative xs-h1 xs-w1 xs-border-none xs-no-fill xs-align-top"><i class="svg--sidebar-arrow svg--arrow xs-center"></i></button>
				</div>
				<div id="js-left-sidebar-content" class="left-sidebar-content xs-p2">
				</div>
				<div id="js-webcam-hub-left-sidebar-bottom" class="left-sidebar-bottom xs-align-bottom xs-absolute xs-t0 xs-r0 xs-l0 sm-b0 sm-t-auto">
					<div class="webcam-iframe-slideshow-buttons webcam-iframe-slideshow-buttons--sidebar xs-relative xs-h1 md-hide">
						<button class="mobile-map-button js-zostrap-show-modal xs-border-left-none xs-border-bottom-none xs-border-top-none xs-no-fill xs-width-full xs-h1 xs-w1 sm-hide" data-modal-id="js-webcam-hub-skirama-modal"><i class="svg--map svg28"></i></button>
						<button class="button-previous js-webcam-hub-button-previous xs-border-right xs-border-top-none xs-border-bottom-none xs-no-fill xs-h1 xs-w1 xs-absolute xs-align-top" data-key=""><i class="svg--caret-left xs-center"></i></button>
						<button class="button-play js-webcam-hub-button-play_pause xs-border-none xs-no-fill xs-h1 xs-w1 xs-absolute xs-mx-auto xs-align-top"><i class="svg--pause xs-center"></i></button>
						<button class="button-next js-webcam-hub-button-next xs-border-left xs-border-top-none xs-border-bottom-none xs-no-fill xs-h1 xs-w1 xs-absolute xs-r0 xs-align-top" data-key=""><i class="svg--caret-right xs-center"></i></button>
					</div>
					<div class="map-block xs-hide sm-block">
						<button class="mobile-map-button js-zostrap-show-modal xs-border-left-none xs-border-bottom-none xs-no-fill xs-width-full xs-h1" data-modal-id="js-webcam-hub-skirama-modal"><i class="svg--map svg28"></i></button>
						<div id="js-webcam-hub-desktop-map" class="desktop-map map-close xs-relative xs-overflow-hidden">
							<div class="desktop-map-open-close xs-z1">
								<i id="js-webcam-hub-map_arrow-open_close" class="svg--map-arrow svg20 pointer"></i>
							</div>
							<img class="xs-width-full" src="">
						</div>
					</div>
				</div>
			</div>
			<div class="gc xs-gc12 sm-gc--auto webcam-hub-main">
				<div id="js-webcam-iframe" class="webcam-iframe">
					<iframe src="" class="js-webcam-hub-webcam_id" width="100%" frameBorder="0" scrolling="no"></iframe>
				</div>
				<div id="js-footer" class="footer grid xs-hide md-flex xs-h1">
					<div class="gc xs-gc--auto xs-mx2 xs-overflow-hidden">
						<div id="js-info-message" class="xs-relative info-message">
						</div>
					</div>
					<div class="webcam-iframe-slideshow-buttons gc xs-gc--none">
						<button class="button-previous js-webcam-hub-button-next xs-border-bottom-none xs-border-top-none xs-no-fill xs-h1 xs-w1 xs-relative xs-float-right" data-key=""><i class="svg--caret-right xs-center"></i></button>
						<button class="button-play js-webcam-hub-button-play_pause xs-border-none xs-no-fill xs-h1 xs-w1 xs-relative xs-float-right"><i class="svg--pause xs-center"></i></button>
						<button class="button-next js-webcam-hub-button-previous xs-border-bottom-none xs-border-top-none xs-no-fill xs-h1 xs-w1 xs-relative xs-float-right" data-key=""><i class="svg--caret-left xs-center"></i></button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="js-webcam-hub-skirama-modal" class="zs-modal js-zostrap-hide-modal">
			<div class="modal__content">
				<div class="modal__body xs-p0" style="">
					<div id="js-webcam-hub-skirama-modal-desktop-map" class="xs-relative xs-overflow-hidden">
						<img class="xs-width-full" src="">
					</div>
				</div>
			</div>
		</div>

		<!-- SCRIPT - LAST -->
		<script type="text/javascript" src="../frontend/script_data/<?php echo $_REQUEST['player_id']?>"></script>
		<script id="webcam-hub-guest" type="text/javascript" src="./js/script-guest.js" data-obj_id="1"></script>
		<!-- /SCRIPT - LAST -->
	</body>
</html>
