/* 
 * Project: webcam-hub.
 * Date-creation: 01/12/2017.
 * Creator: Wequid.
 */
var WQA = WQA || {};
WQA.thisScript = document.querySelector('script#webcam-hub-guest');
WQA.thisScriptsSrc = WQA.thisScript.src;
WQA.container = document.querySelector('#js-webcam-hub');
WQA.viewport_height = window.innerHeight;
WQA.window_message_listeners = {};
WQA.host_old_scroll;
WQA.last_height = null;
WQA.slideshowIsStopped = false;

document.querySelector('#js-webcam-iframe iframe').onload = function() {
	WQA.iframe_isload = true;
}

document.querySelector('#js-webcam-hub-desktop-map img').onload = function() {
	WQA.skirama_isload = true;
}

document.addEventListener('ZoStrap-ready', function(e) {
	WQA.container = ZoStrap.$(WQA.container);
	WQA.obj_id = ZoStrap.$(WQA.thisScript).data('obj_id');

	WQA.resize = function(new_size) {
		var footer_height = 0;
		if (ZoStrap.$('#js-footer').is(":visible")) {
			footer_height = ZoStrap.$('#js-footer').height();
		}
		iFrame_height = Math.min(window.innerWidth / 16 * 9, WQA.viewport_height - footer_height);
		ZoStrap.$('.js-webcam-hub-webcam_id').height(iFrame_height);

		if (ZoStrap.$('#js-left-sidebar').hasClass('transition-open') == false || ZoStrap.$('#js-left-sidebar').hasClass('transition-close') == false) {
			map = ZoStrap.$('#js-webcam-hub-desktop-map');
			if (map.hasClass('map-open')) {
				map_image = map.find('img');
				var size = WQA.getMaxImageSize(map_image);
				map.css({
					'width': size.max_width + 'px',
					'height': size.max_height + 'px',
				});
			}
			var sidebar_container_height = (ZoStrap.$('#js-webcam-iframe').height() + footer_height) - (ZoStrap.$('#js-left-sidebar-open_close').height() + ZoStrap.$('#js-webcam-hub-left-sidebar-bottom').height());
			sidebar_container_height -= (ZoStrap.$('#js-left-sidebar-content').outerHeight(true) - ZoStrap.$('#js-left-sidebar-content').height());
			ZoStrap.$('#js-left-sidebar-content').height(sidebar_container_height);
		}

		if (typeof new_size === typeof undefined) {
			new_size = WQA.container.outerHeight(true);
		}
		if (new_size !== WQA.last_height) {
			WQA.last_height = new_size;
			a = {};
			a.action = 'resize';
			a.data = new_size;
			window.parent.postMessage(JSON.stringify(a), '*');
		}

	}

	WQA.scrollTop = function(b) {
		new_pos = b || 0;
		window.scrollTo(0, new_pos);
		ZoStrap.$('html, body').animate({ scrollTop: WQA.container.offset().top }, 700);
		if (window.self !== window.top) {
			a = { 'action': 'scrollTo', 'data': b };
			window.parent.postMessage(JSON.stringify(a), '*');
		}
	}

	WQA.askParentScroll = function(success_listener) {
		a = { 'action': 'getScrollY' };
		WQA.window_message_listeners['getScrollY'] = success_listener;
		if (window.self !== window.top) window.parent.postMessage(JSON.stringify(a), '*');
	}

	window.addEventListener('message', function(event) {
		try {
			a = JSON.parse(event.data);
			if (typeof WQA.window_message_listeners[a.action] === "function") {
				WQA.window_message_listeners[a.action](a.data);
			}
		} catch (e) {
			console.log('JSON parse exception for:' + event);
		}
	});

	WQA.window_message_listeners['getViewport'] = function(viewportHeight) {
		if (typeof viewportHeight == typeof undefined) {
			WQA.viewport_height = window.innerHeight;
		} else {
			WQA.viewport_height = viewportHeight;
		}
		WQA.height_change_interval = setInterval(function() { WQA.resize(); }, 400);
	};

	a = { 'action': 'getViewport' };
	window.parent.postMessage(JSON.stringify(a), '*');

	if (typeof WQA.webcams != typeof undefined && WQA.webcams.length > 0 && typeof WQA.webcams[WQA.selected_key] != typeof undefined) {
		WQA.moveInfoMessage = function() {
			var info_message_container = ZoStrap.$('#js-info-message');
			if (info_message_container.is(":visible")) {
				var info_message_mover = ZoStrap.$('#js-info-message div:visible');
				if (info_message_mover.length > 0) {
					var info_message = info_message_mover.find('p');
					if (info_message_mover.width() + info_message_mover.position().left <= info_message_container.width()) {
						info_message_mover.append(info_message.clone());
					}
					info_message_mover.css('left', (info_message_mover.position().left - 1) + 'px');
				}
			}
		}

		WQA.moveInfoMessage_id = setInterval(function() { WQA.moveInfoMessage(); }, 30);

		ZoStrap.$('#js-left-sidebar-open_close button').on('click', function(event) {
			var button = ZoStrap.$(this);
			var sidebar = ZoStrap.$('#js-left-sidebar');
			if (button.outerWidth(true) == sidebar.width()) {
				WQA.sidebarOpen()
			} else {
				WQA.sidebarClose(button);
			}
		});

		WQA.sidebarOpen = function() {
			var sidebar = ZoStrap.$('#js-left-sidebar');
			sidebar.removeClass('sidebar-close');
			sidebar.addClass('sidebar-transition-open');
			sidebar.css('width', '');
		}

		WQA.sidebarClose = function(button) {
			var sidebar = ZoStrap.$('#js-left-sidebar');
			sidebar.removeClass('sidebar-open');
			sidebar.addClass('sidebar-transition-close');
			sidebar.css('width', button.outerWidth(true) + 'px');
			map = ZoStrap.$('#js-webcam-hub-desktop-map');
			map.removeClass('map-open map-close map-transition-open map-transition-close').addClass('map-close');
			map.css({
				'width': '',
				'height': '',
			});
		}

		WQA.transitionManager = function(elem, prefix) {
			if (elem.hasClass(prefix + '-transition-open') == true) {
				elem.removeClass(prefix + '-transition-open').addClass(prefix + '-open');
			}
			if (elem.hasClass(prefix + '-transition-close') == true) {
				if (prefix == 'map') {
					map_image = elem.find('img');
					//elem.height(map_image.height());
				}
				elem.removeClass(prefix + '-transition-close').addClass(prefix + '-close');
			}
		}

		ZoStrap.$('#js-left-sidebar').bind('transitionend', function() {
			WQA.transitionManager(ZoStrap.$(this), 'sidebar');
		});

		ZoStrap.$('#js-webcam-hub-map_arrow-open_close').on('click', function(event) {
			map = ZoStrap.$('#js-webcam-hub-desktop-map');
			if (map.hasClass('map-close')) {
				WQA.mapOpen();
			}
			if (map.hasClass('map-open')) {
				WQA.mapClose();
			}
		});

		WQA.mapOpen = function() {
			map = ZoStrap.$('#js-webcam-hub-desktop-map');
			map.removeClass('map-close').addClass('map-transition-open');
			map_image = map.find('img');
			map.height(map_image.height());
			var size = WQA.getMaxImageSize(map_image);
			map.css({
				'width': size.max_width + 'px',
				'height': size.max_height + 'px',
			});
		}

		WQA.mapClose = function() {
			map = ZoStrap.$('#js-webcam-hub-desktop-map');
			map.removeClass('map-open').addClass('map-transition-close');
			map.css({
				'width': '',
				'height': '',
			});
		}

		ZoStrap.$('#js-webcam-hub-desktop-map').bind('transitionend', function() {
			WQA.transitionManager(ZoStrap.$(this), 'map');
		});

		WQA.getMaxImageSize = function(map_image) {
			map_image_factor = map_image.height() / map_image.width();
			max_width = window.innerWidth;
			max_height = max_width * map_image_factor;
			max_image_height = (ZoStrap.$('#js-left-sidebar').outerHeight() - (ZoStrap.$('#js-left-sidebar-content').position().top + ZoStrap.$('#js-left-sidebar-content-title').outerHeight(true)));
			if (max_height > max_image_height) {
				max_height = max_image_height;
				max_width = max_height / map_image_factor;
			}
			return { 'max_height': max_height, 'max_width': max_width };
		}

		ZoStrap.$('.js-webcam-hub-button-play_pause').on('click', function(event) {
			var icon = ZoStrap.$(this).find('i');
			if (icon.hasClass('svg--pause') == true) {
				WQA.slideshowIsStopped = true;
				WQA.stopSlideshow()
			} else {
				WQA.slideshowIsStopped = false;
				WQA.startSlideshow();
			}
		});

		ZoStrap.$('.js-webcam-hub-button-previous').on('click', function(event) {
			var key = parseInt(ZoStrap.$(this).data('key')) - 1;
			WQA.changeWebcam(key);
		});

		ZoStrap.$('.js-webcam-hub-button-next').on('click', function(event) {
			var key = parseInt(ZoStrap.$(this).data('key')) + 1;
			WQA.changeWebcam(key);
		});

		WQA.changeWebcam = function(key) {
			if (ZoStrap.$('[data-key=' + key + ']').length == 0) {
				if (key == -1) {
					key = WQA.webcams.length - 1;
				} else {
					key = 0;
				}
			}
			WQA.selected_key = key;
			if (ZoStrap.$('.js-webcam-hub-button-play_pause i').hasClass('svg--pause')) {
				WQA.stopSlideshow();
				WQA.checkIframeLoad();
			}
			ZoStrap.$('[data-key]:not(.svg--pin, .js-webcam-hub-button-previous, .js-webcam-hub-button-next)').addClass('js-hide');
			ZoStrap.$('.svg--pin[data-key]').removeClass('svg--pin-selected');
			ZoStrap.$('[data-key=' + key + ']').removeClass('js-hide');
			ZoStrap.$('.svg--pin[data-key=' + key + ']').addClass('svg--pin-selected');
			if (WQA.webcams[key].is_image == true) {
				ZoStrap.$('.js-webcam-hub-webcam_id').attr('src', window.location.origin + '/webcam-hub/frontend/webcam/?webcam_id=' + WQA.webcams[key].id);
			} else {
				ZoStrap.$('.js-webcam-hub-webcam_id').attr('src', WQA.webcams[key].url);
			}
			ZoStrap.$('.js-webcam-hub-button-previous').each(function(index, elem) {
				ZoStrap.$(elem).data('key', key);
			});
			ZoStrap.$('.js-webcam-hub-button-next').each(function(index, elem) {
				ZoStrap.$(elem).data('key', key);
			});
			return key;
		}

		WQA.startSlideshow = function() {
			ZoStrap.$('.js-webcam-hub-button-play_pause i').removeClass('svg--play').addClass('svg--pause');
			WQA.slideshow = setTimeout(function() {
				WQA.changeWebcam(WQA.selected_key + 1);
			}, WQA.slideshowInterval);
			//console.log('WQA.slideshow '+WQA.slideshow);
		}

		WQA.stopSlideshow = function() {
			ZoStrap.$('.js-webcam-hub-button-play_pause i').removeClass('svg--pause').addClass('svg--play');
			clearTimeout(WQA.iframeLoad);
			clearTimeout(WQA.slideshow);
		}

		WQA.checkIframeLoad = function() {
			if (!WQA.iframe_isload) {
				WQA.iframeLoad = setTimeout(function() {
					WQA.checkIframeLoad();
				}, 400);
				//console.log('WQA.iframeLoad '+WQA.iframeLoad);
			} else {
				//console.log('IFRAME LOAD. WQA.iframeLoad '+WQA.iframeLoad);
				WQA.startSlideshow();
				WQA.iframe_isload = false;
			}
		}

		ZoStrap.$('#js-webcam-iframe iframe').on('mouseover', function() {
			WQA.stopSlideshow();
		});

		ZoStrap.$('#js-webcam-iframe iframe').on('mouseout', function() {
			if (WQA.slideshowIsStopped == false) {
				WQA.startSlideshow();
			}
		});

		WQA.checkSkiramaLoad = function() {
			if (!WQA.skirama_isload) {
				WQA.skiramaLoad = setTimeout(function() {
					WQA.checkSkiramaLoad();
				}, 400);
			} else {

			}
		}

		WQA.skirama_positioning = function() {
			skirama_bg = ZoStrap.$('#js-webcam-hub-desktop-map img')[0];
			naturalSizeFactor = [100 / skirama_bg.naturalWidth, 100 / skirama_bg.naturalHeight];
			ZoStrap.$('.js-webcam-hub-map-pin').each(function(index, elem) {
				ZoStrap.$(this).removeClass('js-hide');
				var base_size = (typeof ZoStrap.$(this).data('icon_size') != typeof undefined ? ZoStrap.$(this).data('icon_size') : WQA.icon_size);
				ZoStrap.$(this).attr('style', 'top:' + ZoStrap.$(this).data('y') * naturalSizeFactor[1] + '%;left:' + ZoStrap.$(this).data('x') * naturalSizeFactor[0] + '%;width:' + (base_size * naturalSizeFactor[0]) + '%;height:' + (base_size * naturalSizeFactor[1]) + '%;');
			});
		}

		ZoStrap.$('meta[http-equiv="refresh"]').attr('content', WQA.page_refresh);
		ZoStrap.$('#js-webcam-hub-theme').attr('href', './css/themes/' + WQA.theme + '.css');
		if (WQA.webcams[WQA.selected_key].is_image == true) {
			ZoStrap.$('.js-webcam-hub-webcam_id').attr('src', window.location.origin + '/webcam-hub/frontend/webcam/?webcam_id=' + WQA.webcams[WQA.selected_key].id);
		} else {
			ZoStrap.$('.js-webcam-hub-webcam_id').attr('src', WQA.webcams[WQA.selected_key].url);
		}
		ZoStrap.$('.js-webcam-hub-button-previous').data('key', WQA.selected_key);
		ZoStrap.$('.js-webcam-hub-button-next').data('key', WQA.selected_key);
		ZoStrap.$('#js-webcam-hub-desktop-map img').attr('src', WQA.skirama);
		ZoStrap.$('#js-webcam-hub-skirama-modal-desktop-map img').attr('src', WQA.skirama);
		for (var index in WQA.webcams) {
			var webcam = WQA.webcams[index];
			ZoStrap.$('#js-left-sidebar-content').append(
				'<article class="' + (WQA.selected_key == index ? '' : 'js-hide') + '" data-key="' + index + '">' +
				'<h2 id="js-left-sidebar-content-title">' + webcam.name + '</h2>' +
				webcam.content +
				'</article>'
			);
			ZoStrap.$('#js-webcam-hub-desktop-map').append(
				'<i class="svg--pin ' + (WQA.selected_key == index ? 'svg--pin-selected' : '') + ' pointer xs-absolute js-webcam-hub-map-pin"' +
				'data-x="' + webcam.skirama_position_x + '"' +
				'data-y="' + webcam.skirama_position_y + '"' +
				'data-key="' + index + '">' +
				'</i>'
			);
			ZoStrap.$('#js-webcam-hub-skirama-modal-desktop-map').append(
				'<i class="svg--pin ' + (WQA.selected_key == index ? 'svg--pin-selected' : '') + ' pointer xs-absolute js-webcam-hub-map-pin"' +
				'data-x="' + webcam.skirama_position_x + '"' +
				'data-y="' + webcam.skirama_position_y + '"' +
				'data-key="' + index + '">' +
				'</i>'
			);
			ZoStrap.$('#js-info-message').append(
				'<div class="xs-absolute xs-nowrap ' + (WQA.selected_key == index ? '' : 'js-hide') + '" data-key="' + index + '">' +
				'<p>' + webcam.info_message + '</p>' +
				'</div>'
			);
		}

		if (WQA.slideshow_autoStart) {
			WQA.checkIframeLoad();
		} else {
			ZoStrap.$('.js-webcam-hub-button-play_pause i').removeClass('svg--pause').addClass('svg--play');
			WQA.slideshowIsStopped = true;
		}

		ZoStrap.$('.js-webcam-hub-map-pin').on('click', function(event) {
			var pin = ZoStrap.$(this);
			WQA.changeWebcam(pin.data('key'));
			if (ZoStrap.$('#js-webcam-hub-desktop-map').hasClass('map-open')) {
				WQA.mapClose();
			}
			if (ZoStrap.$('#js-webcam-hub-skirama-modal').is(":visible") == true) {
				ZoStrap.hideModal('#js-webcam-hub-skirama-modal');
			}
		});

		ZoStrap.$('#js-webcam-hub-desktop-map img').one("load", function() {
			WQA.skirama_positioning();
		}).each(function() {
			if (this.complete) ZoStrap.$(this).load();
		});

	} else {
		ZoStrap.$('#js-webcam-hub').removeClass('grid webcam-hub').addClass('left-sidebar xs-p2').html('<h2 class="xs-m2">Player not Found</h2>');
	}

});

// IMPORTANT THIS MUST BE THE LAST OPERATION
ZoStrap.init();