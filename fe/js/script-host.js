/* 
 * Project: webcam-hub.
 * Date-creation: 01/12/2017.
 * Creator: Wequid.
 */
var thisScript = document.querySelector('script#js-webcam-hub-host-script');
var iFrame = document.getElementById(thisScript.dataset.iframe_id);

function wqa_getLocation(href) {
	var location = document.createElement("a");
	location.href = href;
	// IE doesn't populate all link properties when setting .href with a relative URL,
	// however .href will return an absolute URL which then can be used on itself
	// to populate these additional fields.
	if (location.host == "") {
		location.href = location.href;
	}
	return location;
};

function wqa_cumulativeOffset(element) {
	var top = 0, left = 0;
	do {
		top += element.offsetTop  || 0;
		left += element.offsetLeft || 0;
		element = element.offsetParent;
	} while(element);

	return {
		top: top,
		left: left
	};
};

window.addEventListener('message', function(event){
	if (~event.origin.indexOf((wqa_getLocation(iFrame.src)).hostname))
	{
		a = JSON.parse(event.data);
		if (a.action=='resize') iFrame.height = a.data;
		if (a.action=='scrollTo'){
			scrollY = a.data || wqa_cumulativeOffset(iFrame).top;
			scrollY -= (thisScript.getAttribute('data-scrolltop-bias') || 105);
			window.scrollTo(0, scrollY);
		}
		if (a.action=='getScrollY'){
			b = {'action': 'getScrollY', 'data': (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0)};
			iFrame.contentWindow.postMessage(JSON.stringify(b), '*');
		}
		if (a.action=='getViewport'){
			b = {'action': 'getViewport', 'data': window.innerHeight};
			iFrame.contentWindow.postMessage(JSON.stringify(b), '*');
		}
	}
});
