// Modal in Multi z-indez
$(document).on('show.bs.modal', '.modal', function (event) {
	var zIndex = 1050 + (10 * $('.modal:visible').length);
	$(this).css('z-index', zIndex);
});
$(document).on('hidden.bs.modal', '.modal', function (event) {
	if ($('.modal:visible').length > 0)
	{
		$('body').addClass('modal-open');
	}
});
// /Modal

// Switchery
function changeSwitcheryState(el,value){
	if($(el).is(':checked')!=value){
		$(el).trigger("click")
	}
}
// /Switchery

// Loader
var spinner = document.querySelector('#js-loader-template .loader-spinner')
var background = document.querySelector('#js-loader-template .loader-background')
var line = document.querySelector('#js-loader-template .loader-line')
var tick = document.querySelector('#js-loader-template .loader-tick')
var wrong = document.querySelector('#js-loader-template .loader-wrong')
var arrows = document.querySelectorAll('#js-loader-template .loader-arrow')

function loader_show_in(container) {
	loader_prepare(container);
	$('#js-loader-template').addClass('active');
}

function loader_hide_in(container) {
	loader_prepare(container);
	$('#js-loader-template').removeClass('active');
}

function loader_prepare(container)
{
	if (typeof container != typeof undefined)
	{
		container = $(container);
		$('#js-loader-template').appendTo(container);
	} else
	{
		$('#js-loader-template').appendTo('body');
	}
	loader_reset();
}

function loader_start() {
	// Show the spinner
	dynamics.animate(spinner, {
		opacity: 1
	}, {
		duration: 0,
		complete: loader_animateLine
	})
}

// This rotate the background (circle+arrows) indefinitely
function loader_rotate() {
	dynamics.animate(background, {
		rotateZ: 180,
		rotateC: 60
	}, {
		type: dynamics.linear,
		duration: 500,
		complete: function() {
			dynamics.css(background, { rotateZ: 0 })
			loader_rotate()
		}
	})
}

// Animate the line
function loader_animateLine() {
	dynamics.animate(line, {
		strokeDasharray: "40, 117"
	}, {
		type: dynamics.easeInOut,
		duration: 400,
		friction: 700,
		complete: function() {
			dynamics.animate(line, {
				strokeDasharray: "120, 37"
			}, {
				type: dynamics.easeInOut,
				duration: 800,
				complete: loader_animateLine
			})
		}
	})
}

// Animate the success state
function loader_animateSuccess() {
	// First, we animate the line to form a whole circle
	dynamics.animate(line, {
		strokeDasharray: "157, 0",
	}, {
		type: dynamics.easeIn,
		duration: 500,
		friction: 200,
		complete: function() {
			// Then we change the line color and make it a full circle
			// by increasing the strokeWidth
			dynamics.animate(line, {
				strokeWidth: 100,
				stroke: "#0AB000"
			}, {
				friction: 200,
				duration: 300
			})
			// // We hide the arrows
			dynamics.animate(arrows, {
				fill: "#0AB000",
				translate: 5.5,
				scale: 0.5
			}, {
				friction: 200,
				duration: 300
			})
			// Reset wrong to originals
			dynamics.css(wrong, {
				opacity: 0,
				rotateZ: -45,
				rotateC: 60
			})
			// Animate the tick icon
			dynamics.animate(tick, {
				opacity: 1,
				rotateZ: 0,
				rotateC: 60
			}, {
				type: dynamics.spring,
				friction: 300,
				duration: 1000,
				delay: 300
			})
		}
	})
}

// Animate the error state
function loader_animateError() {
	// First, we animate the line to form a whole circle
	dynamics.animate(line, {
		strokeDasharray: "157, 0",
	}, {
		type: dynamics.easeIn,
		duration: 500,
		friction: 200,
		complete: function() {
			// Then we change the line color and make it a full circle
			// by increasing the strokeWidth
			dynamics.animate(line, {
				strokeWidth: 100,
				stroke: "#B00000"
			}, {
				friction: 200,
				duration: 300
			})
			// // We hide the arrows
			dynamics.animate(arrows, {
				fill: "#B00000",
				translate: 5.5,
				scale: 0.5
			}, {
				friction: 200,
				duration: 300
			})
			// Reset tick to originals
			dynamics.css(tick, {
				opacity: 0,
				rotateZ: -45,
				rotateC: 60
			})
			// Animate the wrong icon
			dynamics.animate(wrong, {
				opacity: 1,
				rotateZ: 0,
				rotateC: 60
			}, {
				type: dynamics.spring,
				friction: 300,
				duration: 1000,
				delay: 300
			})
		}
	})
}

// Reset the whole animation
function loader_reset() {
	// Reset css properties to originals
	dynamics.css(tick, {
		opacity: 0,
		rotateZ: -45,
		rotateC: 60
	})
	dynamics.css(wrong, {
		opacity: 0,
		rotateZ: -45,
		rotateC: 60
	})
	dynamics.css(line, {
		strokeDasharray: "120, 37",
		stroke: "#0083FF",
		strokeWidth: 10
	})
	dynamics.css(arrows, {
		opacity: 1,
		fill: "#0083FF",
		scale: 1
	})
}

// Start!
loader_start()
loader_rotate()
// /Loader
