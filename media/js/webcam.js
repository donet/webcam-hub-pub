/*!
 * Webcam Library v1.0
 * http://wequid.com
 *
 * Copyright (c) 2017-2020 wequid srl
 * Author Massimiliano Giorgio Gasti
 *
 * Date: 2018-02-08T10:00Z
 */

(function ($, window, document, undefined) {
	"use strict";
	
	var $body = $('body'),
		$window = $(window),
		$document = $(document);
	
	//constants
	var IMG_LEFT_ANIMATE_DIRECTION = 1; //direction must be 1 or -1
	
	//ease out method
	/*
		t : current time,
		b : intial value,
		c : changed value,
		d : duration
	*/
	function easeOutQuart(t, b, c, d) {
		t /= d;
		t--;
		return -c * (t * t * t * t - 1) + b;
	};
	
	//function to check if image is loaded
	function imageLoaded(img) {
		return img.complete && (typeof img.naturalWidth === 'undefined' || img.naturalWidth !== 0);
	}
	
	function WebcamViewer(container, imgElm, options) {
		var self = this;

		self.container = container;
		self.imgElm = imgElm;
		options = self.options = $.extend({}, WebcamViewer.defaults, options);
		container.addClass('webcam_viewer-container');
		if (container.css('position') == 'static') container.css('position', 'relative');
		self._viewerId = 'webcam_viewer' + Math.floor(Math.random() * 1000000);
	}
	
	WebcamViewer.prototype = {
		constructor: WebcamViewer,
		_init: function () {
			var viewer = this,
				options = viewer.options,
				container = this.container,
				imgElm = this.imgElm;

			var eventSuffix = '.' + viewer._viewerId;
			this.eventSuffix = eventSuffix;
			
			self.zoomValue = options.wheelZoomMin;
			
			if (viewer.options.animateAutoStart)
			{
				viewer.animateStart = true;
			} else
			{
				viewer.animateStart = false;
			}

			viewer.load();
		},
		mouseWheelAnimate: function ()
		{
			var self = this,
				imgElm = this.imgElm,
				eventSuffix = this.eventSuffix;
			
			if (self.options.wheelScroll)
			{
				/*Add scroll interation in mouse wheel*/
				imgElm.on("mousewheel" + eventSuffix + " DOMMouseScroll" + eventSuffix, function (e) {
					if (!self.loaded) return;
					
					// cross-browser wheel delta
					var delta = Math.max(-1, Math.min(1, (e.originalEvent.wheelDelta || -e.originalEvent.detail))),
						imgLeft = parseInt(imgElm.css('left')),
						scrollValue = imgLeft+(self.options.wheelScrollValue * delta),
						imgLeftLimit = -(imgElm.innerWidth()-self.container.innerWidth());
					
					//A mouse delta after which it should stop preventing default behaviour of mouse wheel
					if(!(scrollValue <= 0 && scrollValue >= imgLeftLimit)){
						return;
					}
					e.preventDefault();
					
					if (delta > 0)
					{
						IMG_LEFT_ANIMATE_DIRECTION = -1;
					} else
					{
						IMG_LEFT_ANIMATE_DIRECTION = 1;
					}

					var contOffset = self.container.offset(),
						x = (e.pageX || e.originalEvent.pageX) - contOffset.left,
						y = (e.pageY || e.originalEvent.pageY) - contOffset.top;
					
					self.moveImgLeft(scrollValue);
				});
			}else if (self.options.wheelZoom)
			{
				/*Add zoom interation in mouse wheel*/
				imgElm.on("mousewheel" + self.eventSuffix + " DOMMouseScroll" + self.eventSuffix, function (e) {
					if (!self.loaded) return;
					
					// cross-browser wheel delta
					var delta = Math.max(-1, Math.min(1, (e.originalEvent.wheelDelta || -e.originalEvent.detail))),
						zoomValue = self.options.wheelZoomMin * (100 + delta * self.options.wheelZoomSpeed) / 100;
					
					if(!(zoomValue <= 100 && zoomValue >= self.options.wheelZoomMax)){
						//return;
					}
					e.preventDefault();
					
					var contOffset = self.container.offset(),
						x = (e.pageX || e.originalEvent.pageX) - contOffset.left,
						y = (e.pageY || e.originalEvent.pageY) - contOffset.top;
					
					self.zoom(zoomValue, {
						x: x,
						y: y
					});
				});
			}
		},
		moveImgLeftAnimate: function ()
		{
			var self = this;
			if (self.animateStart)
			{
				self.animateStart = true;
				var scrollValue = parseInt(self.imgElm.css('left'))-(self.options.animateScrollValue*IMG_LEFT_ANIMATE_DIRECTION);
				if (!self.loaded) return;
				
				self.moveImgLeft(scrollValue);
				self.moveImgLeftAnimateTimeout = setTimeout(function(){self.moveImgLeftAnimate();}, self.options.animateScrollSpeed);
			}
		},
		moveImgLeftAnimateStart: function ()
		{
			var self = this;
			self.animateStart = true;
			self.moveImgLeftAnimate();
		},
		moveImgLeftAnimateStop: function ()
		{
			var self = this;
			self.animateStart = false;
		},
		moveImgLeftAnimateToggle: function ()
		{
			var self = this;
			if (self.animateStart)
			{
				self.moveImgLeftAnimateStop();
			} else
			{
				self.moveImgLeftAnimateStart();
			}
		},
		moveImgLeft: function (moveLeft)
		{
			var self = this;
			if (!self.loaded) return;
			var imgLeftLimit = -(this.imgElm.innerWidth()-this.container.innerWidth())
			if (moveLeft >= 0)
			{
				moveLeft = 0;
				IMG_LEFT_ANIMATE_DIRECTION = 1;
			} else if (moveLeft <= imgLeftLimit)
			{
				moveLeft = imgLeftLimit;
				IMG_LEFT_ANIMATE_DIRECTION = -1;
			}
			
			this.imgElm.css({
				left: moveLeft,
			});
		},
		dradImgInteration: function ()
		{
			var self = this;
			if (typeof $.fn.draggable != typeof undefined)
			{
				self.imgElm.draggable({
					scroll: false,
					drag: function( event, ui ) {
						var imgLeftLimit = -(ui.helper.innerWidth()-ui.helper.parent().innerWidth());
						var imgTopLimit = -(ui.helper.innerHeight()-ui.helper.parent().innerHeight());
						if (ui.position.left >= 0)
						{
							ui.position.left = 0;
						} else if (ui.position.left <= imgLeftLimit)
						{
							ui.position.left = imgLeftLimit;
						}
						if (ui.position.top >= 0)
						{
							ui.position.top = 0;
						} else if (ui.position.top <= imgTopLimit)
						{
							ui.position.top = imgTopLimit;
						}
						if (ui.position.left > ui.originalPosition.left)
						{
							IMG_LEFT_ANIMATE_DIRECTION = -1;
						} else
						{
							IMG_LEFT_ANIMATE_DIRECTION = 1;
						}
					}
				});
			}
		},
		//method to zoom images NOT IMPLEMENTED
		zoom: function (perc, point)
		{
			//perc = Math.round(Math.max(this.options.wheelZoomMin, perc));
			//perc = Math.min(this.options.wheelZoomMax, perc);

			point = point || {
				x: this.containerDim.w / 2,
				y: this.containerDim.h / 2
			};

			var self = this,
				maxZoom = this.options.wheelZoomMax,
				curPerc = this.zoomValue,
				curImg = this.imgElm,
				containerDim = this.containerDim,
				curLeft = parseFloat(curImg.css('left')),
				curTop = parseFloat(curImg.css('top'));

			var step = 0;

			//calculate base top,left,bottom,right
			var containerDim = self.containerDim,
				imageDim = self.imageDim;
			var baseLeft = (containerDim.w - imageDim.w) / 2,
				baseTop = (containerDim.h - imageDim.h) / 2,
				baseRight = containerDim.w - baseLeft,
				baseBottom = containerDim.h - baseTop;
			
			function zoom() {
				step++;

				if (step < 20) {
					self._zoomFrame = requestAnimationFrame(zoom);
				}

				var tickZoom = easeOutQuart(step, curPerc, perc - curPerc, 20);

				var ratio = tickZoom / curPerc,
				imgWidth = self.imageDim.w * tickZoom / 100,
				imgHeight = self.imageDim.h * tickZoom / 100,
				newLeft = -((point.x - curLeft) * ratio - point.x),
				newTop = -((point.y - curTop) * ratio - point.y);

				//fix for left and top
				newLeft = Math.min(newLeft, baseLeft);
				newTop = Math.min(newTop, baseTop);

				//fix for right and bottom
				if((newLeft + imgWidth) < baseRight){
					newLeft = baseRight - imgWidth; //newLeft - (newLeft + imgWidth - baseRight)
				}

				if((newTop + imgHeight) < baseBottom){            
					newTop =  baseBottom - imgHeight; //newTop + (newTop + imgHeight - baseBottom)
				}

				curImg.css({
					height: imgHeight + 'px',
					width: imgWidth + 'px',
					left: newLeft + 'px',
					top: newTop + 'px'
				});

				self.zoomValue = tickZoom;

				//self._resizeHandle(imgWidth, imgHeight, newLeft, newTop);

				//update zoom handle position
				//self.zoomHandle.css('left', ((tickZoom - 100) * self._zoomSliderLength) / (maxZoom - 100) + 'px');
			}

			//zoom();
		},
		clickImgInteration: function()
		{
			var self = this;
			//handle double tap for zoom in and zoom out
			var touchtime = 0,
				point;
			self.imgElm.on('click' + self.eventSuffix, function (e) {
				if (touchtime == 0) {
					touchtime = Date.now();
					point = {
						x: e.pageX,
						y: e.pageY
					};
				} else {
					if ((Date.now() - touchtime) < 500 && Math.abs(e.pageX - point.x) < 50 && Math.abs(e.pageY - point.y) < 50) {
						self.moveImgLeftAnimateToggle();
						touchtime = 0;
					} else {
						touchtime = 0;
					}
				}
			});
		},
		calculateDimensions: function ()
		{
			var self = this,
				imageWidth = self.imgElm.width(),
				imageHeight = self.imgElm.height(),
				contWidth = self.container.width(),
				contHeight = self.container.height();
			//set the container dimension
			self.containerDim = {
				w: contWidth,
				h: contHeight
			}
			//set the image dimension
			var imgWidth, imgHeight, ratio = imageWidth / imageHeight;
			imgWidth = (imageWidth > imageHeight && contHeight >= contWidth) || ratio * contHeight > contWidth ? contWidth : ratio * contHeight;
			imgHeight = imgWidth / ratio;
			self.imageDim = {
				w: imgWidth,
				h: imgHeight
			}
		},
		load: function()
		{
			var self = this,
			container = this.container;
			var currentImg = this.currentImg = this.imgElm;
			self.loaded = false;
			
			function setLoaded()
			{
				self.loaded = true;
				self.calculateDimensions();
				self.mouseWheelAnimate();
				self.moveImgLeftAnimate();
				self.dradImgInteration();
				self.clickImgInteration();
			}
			
			if (imageLoaded(currentImg[0])) {
				setLoaded();
			} else {
				$(currentImg[0]).on('load', setLoaded);
			}
		}
	}
	
	WebcamViewer.defaults = {
		wheelScroll: true, //use for active scroll on mouse wheel
		wheelScrollValue: 70, //increase or decrease value for scroll on mouse wheel
		wheelZoom: false, //use for active scroll on mouse wheel
		wheelZoomMin: 100, //increase or decrease min value for zoom on mouse wheel
		wheelZoomSpeed: 15, //increase or decrease speed for zoom on mouse wheel
		wheelZoomMax: 350, //increase or decrease max value for zoom on mouse wheel
		animateAutoStart: true,
		animateScrollValue: 1,
		animateScrollSpeed: 20,
		refreshOnResize: true, // NOT IMPLEMENTED
	}
	
	window.WebcamViewer = function (container, options) {
		var imgElm, imgSrc;
		if (!(container && (typeof container == "string" || container instanceof Element || container[0] instanceof Element))) {
			options = container;
			container = $('#js-webcam_viewer-container');
		}

		container = $(container);

		if (container.is('img')) {
			imgElm = container;
			imgSrc = imgElm[0].src;
			container = imgElm.wrap('<div id="" class="webcam_viewer-container js-webcam_viewer-container"></div>').parent();
			imgElm.addClass('webcam_viewer-img');
		} else {
			imgSrc = container.attr('src') || container.attr('data-src');
			imgElm = $('<img id="" class="webcam_viewer-img js-webcam_viewer-img" src="'+imgSrc+'" alt="">');
			conatainer.append(imgElm);
		}
		
		imgElm.css({
			position: 'relative',
			zIndex: 1
		});

		var viewer = new WebcamViewer(container, imgElm, options);
		viewer._init();

		return viewer;
	};


	$.fn.WebcamViewer = function (options) {
		return this.each(function () {
			var $this = $(this);
			var viewer = window.WebcamViewer($this, options);
			$this.data('WebcamViewer', viewer);
		});
	}

}((window.jQuery), window, document));
